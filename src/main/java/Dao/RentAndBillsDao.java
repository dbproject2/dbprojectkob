package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.CostRentReport;
import model.RentAndBills;

public class RentAndBillsDao implements Dao<RentAndBills> {

    @Override
    public RentAndBills get(int id) {
        RentAndBills RentAndBills = null;
        String sql = "SELECT * FROM RentAndBills WHERE id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                RentAndBills = RentAndBills.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return RentAndBills;
    }

    @Override
    public List<RentAndBills> getAll() {
        ArrayList<RentAndBills> list = new ArrayList();
        String sql = "SELECT * FROM RentAndBills";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RentAndBills rentAndBills = RentAndBills.fromRS(rs);
                list.add(rentAndBills);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<RentAndBills> getAllOrderBy(String where, String order) {
        ArrayList<RentAndBills> list = new ArrayList();
        String sql = "SELECT * FROM RentAndBills where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RentAndBills rentAndBills = RentAndBills.fromRS(rs);
                list.add(rentAndBills);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<RentAndBills> getAll(String order) {
        ArrayList<RentAndBills> list = new ArrayList();
        String sql = "SELECT * FROM RentAndBills ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RentAndBills rentAndBills = RentAndBills.fromRS(rs);
                list.add(rentAndBills);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public RentAndBills save(RentAndBills obj) {
        String sql = "INSERT INTO RentAndBills (rent_cost, electric_cost, water_cost, user_id)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getRentCost());
            stmt.setDouble(2, obj.getElectricCost());
            stmt.setDouble(3, obj.getWaterCost());
            stmt.setInt(4, obj.getUserId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public RentAndBills update(RentAndBills obj) {
        String sql = "UPDATE RentAndBills"
                + " SET rent_cost = ?, electric_cost = ?, water_cost = ?, user_id = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getRentCost());
            stmt.setDouble(2, obj.getElectricCost());
            stmt.setDouble(3, obj.getWaterCost());
            stmt.setInt(4, obj.getUserId());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(RentAndBills obj) {
        String sql = "DELETE FROM RentAndBills WHERE id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<CostRentReport> getCostRent(String begin, String end) {
        ArrayList<CostRentReport> list = new ArrayList();
        String sql = """
                     SELECT STRFTIME('%Y-%m-%d',"created_date") AS Month,
                            rent_cost + electric_cost + water_cost AS Rent_Cost
                       FROM RentAndBills
                     WHERE STRFTIME('%Y-%m-%d', created_date) BETWEEN ? AND ?
                      ORDER BY Month DESC""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CostRentReport obj = CostRentReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<RentAndBills> getDateDesc(String begin, String end) {
        ArrayList<RentAndBills> list = new ArrayList();
        String sql = """
                     SELECT * FROM RentAndBills
                       WHERE STRFTIME('%Y-%m-%d', created_date) BETWEEN ? AND ?
                       ORDER BY created_date DESC;""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RentAndBills obj = RentAndBills.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
