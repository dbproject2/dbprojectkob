package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.ReceiptDetail;
import model.ReceiptDetailReport;

public class ReceiptDetailDao implements Dao<ReceiptDetail> {

    @Override
    public ReceiptDetail get(int id) {
        ReceiptDetail recieptDetail = null;
        String sql = "SELECT * FROM OrderList WHERE orderlist_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                recieptDetail = ReceiptDetail.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recieptDetail;
    }

    @Override
    public List<ReceiptDetail> getAll() {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM OrderList";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ReceiptDetail> getAllOrderBy(String where, String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM OrderList where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReceiptDetail> getAll(String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM OrderList ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptDetail save(ReceiptDetail obj) {
        String sql = "INSERT INTO OrderList (total_price, qty, pro_id, rec_id)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotalPrice());
            stmt.setInt(2, obj.getQty());
            stmt.setInt(3, obj.getProId());
            stmt.setInt(4, obj.getRecId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ReceiptDetail update(ReceiptDetail obj) {
        String sql = "UPDATE OrderList"
                + " SET total_price = ?, qty = ?, pro_id = ?, rec_id = ?"
                + " WHERE orderlist_id = ?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotalPrice());
            stmt.setInt(2, obj.getQty());
            stmt.setInt(3, obj.getProId());
            stmt.setInt(4, obj.getRecId());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptDetail obj) {
        String sql = "DELETE FROM OrderList WHERE orderlist_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<ReceiptDetailReport> GetTopTenProductSales() {
        ArrayList<ReceiptDetailReport> list = new ArrayList();
        String sql = """
                   SELECT Product.pro_id AS ID, Product.pro_name AS Name, SUM(OrderList.total_price) AS Sales, SUM(OrderList.qty) AS Qty
                     FROM Product
                     JOIN OrderList ON Product.pro_id = OrderList.pro_id
                     GROUP BY Product.pro_id
                     ORDER BY SUM(OrderList.qty) DESC
                     LIMIT 10""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ReceiptDetailReport obj = ReceiptDetailReport.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List< ReceiptDetail> getAllByRecId(int recId) {
        ArrayList< ReceiptDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM OrderList WHERE rec_id = ?";
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, recId);
            rs = stmt.executeQuery();

            while (rs.next()) {
                ReceiptDetail receiptDetail = ReceiptDetail.fromRS(rs);
                list.add(receiptDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    System.out.println("Error closing ResultSet: " + e.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    System.out.println("Error closing PreparedStatement: " + e.getMessage());
                }
            }
        }
        return list;
    }

}
