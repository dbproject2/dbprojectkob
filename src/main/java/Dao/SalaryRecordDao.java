package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.SalaryRecord;

public class SalaryRecordDao implements Dao<SalaryRecord> {

    @Override
    public SalaryRecord get(int id) {
        SalaryRecord checkInOut = null;
        String sql = "SELECT * FROM SalaryRecord WHERE record_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                checkInOut = SalaryRecord.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInOut;
    }

    @Override
    public List<SalaryRecord> getAll() {
        ArrayList<SalaryRecord> list = new ArrayList();
        String sql = "SELECT * FROM SalaryRecord";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SalaryRecord checkInOut = SalaryRecord.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SalaryRecord> getAll(String order) {
        ArrayList<SalaryRecord> list = new ArrayList();
        String sql = "SELECT * FROM SalaryRecord  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SalaryRecord checkInOut = SalaryRecord.fromRS(rs);
                list.add(checkInOut);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    @Override
    public SalaryRecord save(SalaryRecord obj) {
        String sql = "INSERT INTO SalaryRecord (total_Hrs,total_pay,user_id)"
                + "VALUES(?,?,?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getTotal_Hr());
            stmt.setInt(2,obj.getTotal_Pay());
            stmt.setInt(3,obj.getUserId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public int delete(SalaryRecord obj) {
        String sql = "DELETE FROM SalaryRecord WHERE record_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public SalaryRecord update(SalaryRecord obj) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public List<SalaryRecord> getAllOrderBy(String name, String order) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
