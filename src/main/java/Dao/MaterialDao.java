package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.CostBillReport;
import model.Material;
import model.MaterialReport;

public class MaterialDao implements Dao<Material> {

    @Override
    public Material get(int id) {
        Material material = null;
        String sql = "SELECT * FROM Material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                material = Material.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return material;
    }

    public Material getByName(String name) {
        Material material = null;
        String sql = "SELECT * FROM Material WHERE mat_name=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                material = Material.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return material;
    }

    @Override
    public List<Material> getAll() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Material> getAllOrderBy(String where, String order) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Material> getAll(String order) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Material save(Material obj) {
        String sql = "INSERT INTO Material (mat_name, mat_revdate, mat_qty, mat_min, mat_unit, mat_price_per_unit)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getRevDate());
            stmt.setFloat(3, obj.getQty());
            stmt.setFloat(4, obj.getMin());
            stmt.setString(5, obj.getUnit());
            stmt.setInt(6, obj.getPricePerUnit());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Material update(Material obj) {
        String sql = "UPDATE Material"
                + " SET mat_name = ?, mat_revdate = ?, mat_qty = ?, mat_min = ?, mat_unit = ?, mat_price_per_unit = ?"
                + " WHERE mat_id = ?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getRevDate());
            stmt.setFloat(3, obj.getQty());
            stmt.setFloat(4, obj.getMin());
            stmt.setString(5, obj.getUnit());
            stmt.setInt(6, obj.getPricePerUnit());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Material obj) {
        String sql = "DELETE FROM Material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

     public List<MaterialReport> GetLowInStock() {
        ArrayList<MaterialReport> list = new ArrayList();
        String sql = """
                   select mat_name AS Name, mat_qty AS CurrentAmount, mat_min AS Minimum
                     from Material
                     where mat_qty < mat_min""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                MaterialReport obj = MaterialReport.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     public List<CostBillReport> getCostBill(String begin, String end) {
        ArrayList<CostBillReport> list = new ArrayList();
        String sql = """
                     SELECT STRFTIME('%Y-%m-%d',"bill_datetime") AS Month, sum(bill_total_price) AS Material_Cost
                                  FROM Bill
                                  WHERE STRFTIME('%Y-%m-%d', bill_datetime) BETWEEN ? AND ?
                                  ORDER BY Month DESC""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CostBillReport obj = CostBillReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
