package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Promotion;

public class PromotionDao implements Dao<Promotion> {

    @Override
    public Promotion get(int id) {
        Promotion promotion = null;
        String sql = "SELECT * FROM Promotion WHERE promo_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;
    }

    @Override
    public List<Promotion> getAllOrderBy(String where, String order) {
        ArrayList<Promotion> promo_list = new ArrayList();
        String sql = "SELECT * FROM Promotion where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                promo_list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promo_list;
    }

    public Promotion getByName(String name) {
        Promotion promotion = null;
        String sql = "SELECT * FROM Promotion WHERE promo_name=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;
    }

    @Override
    public List<Promotion> getAll() {
        ArrayList<Promotion> promo_list = new ArrayList();
        String sql = "SELECT * FROM Promotion";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                promo_list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promo_list;
    }

    public List<Promotion> getAll(String order) {
        ArrayList<Promotion> promo_list = new ArrayList();
        String sql = "SELECT * FROM Promotion  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                promo_list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promo_list;
    }

    @Override
    public Promotion save(Promotion obj) {
        String sql = "INSERT INTO Promotion (promo_name, promo_starttime, promo_endtime, promo_discount)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setObject(2, Timestamp.valueOf(obj.getStartTime()));
            stmt.setObject(3, Timestamp.valueOf(obj.getEndTime()));
            stmt.setDouble(4, obj.getDiscount());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Promotion update(Promotion obj) {
        String sql = "UPDATE Promotion"
                + " SET promo_name = ?, promo_starttime = ?, promo_endtime = ?, promo_discount = ?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setObject(2, Timestamp.valueOf(obj.getStartTime()));
            stmt.setObject(3, Timestamp.valueOf(obj.getEndTime()));
            stmt.setDouble(4, obj.getDiscount());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Promotion obj) {
        String sql = "DELETE FROM Promotion WHERE promo_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
