package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.CheckInOut;
import model.CostEmpReport;

public class CheckInOutDao implements Dao<CheckInOut> {

    @Override
    public CheckInOut get(int id) {
        CheckInOut checkInOut = null;
        String sql = "SELECT * FROM Checkin_out WHERE checkIO_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkInOut = CheckInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInOut;
    }

    @Override
    public List<CheckInOut> getAll() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM Checkin_out";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInOut> getAll(String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM Checkin_out  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInOut> getAllByUserId(int userId) {
        ArrayList<CheckInOut> list = new ArrayList<>();
        String sql = "SELECT * FROM Checkin_out WHERE user_id = ?";
        Connection conn = DatabaseHelper.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userId);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    System.out.println("Error closing ResultSet: " + e.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    System.out.println("Error closing PreparedStatement: " + e.getMessage());
                }
            }
        }
        return list;
    }

    @Override
    public CheckInOut save(CheckInOut obj) {

        String sql = "INSERT INTO Checkin_out (user_id,salary_id)"
                + "VALUES(?,?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.setInt(2, obj.getSalaryId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE Checkin_out"
                + " SET  checkIO_timeOut = CURRENT_TIMESTAMP ,checkIO_totalHr = ?"
                + " WHERE checkIO_id = ?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getTotalHour());
            stmt.setInt(2, obj.getId());
            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInOut obj) {
        String sql = "DELETE FROM Checkin_out WHERE checkIO_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public int deleteAllById(int id) {
        String sql = "DELETE FROM Checkin_out WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckInOut> getAllOrderBy(String name, String order) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<CostEmpReport> getCostEmp(String begin, String end) {
        ArrayList<CostEmpReport> list = new ArrayList();
        String sql = """
                     SELECT STRFTIME('%Y-%m-%d',"salary_date") AS Month, round(sum(salary_workhr * user_wage),2) AS Employee_Salary_Cost
                            FROM (SummarySalary NATURAL JOIN Checkin_out) NATURAL JOIN User
                            WHERE SummarySalary.salary_id = Checkin_out.salary_id AND Checkin_out.user_id = User.user_id AND STRFTIME('%Y-%m-%d', salary_date) BETWEEN ? AND ?
                            GROUP BY salary_date
                            ORDER BY Month DESC""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CostEmpReport obj = CostEmpReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInOut> getAllByDate(Date date) {
        List<CheckInOut> checkInOuts = new ArrayList<>();
        Connection conn = DatabaseHelper.getConnection();

        if (conn == null) {
            return checkInOuts;
        }

        String sql = "SELECT * FROM Checkin_out WHERE DATE(checkIO_date) = ?";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = dateFormat.format(date);
            stmt.setString(1, formattedDate);

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    CheckInOut checkInOut = CheckInOut.fromRS(rs);
                    checkInOuts.add(checkInOut);
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return checkInOuts;
    }
}
