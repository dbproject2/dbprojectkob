package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.EmployeeReport;
import model.User;

public class UserDao implements Dao<User> {

    @Override
    public User get(int id) {
        User user = null;
        String sql = "SELECT * FROM User WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    public User getByName(String name) {
        User user = null;
        String sql = "SELECT * FROM User WHERE user_name=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }


    @Override
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM User";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<User> getAllOrderBy(String where, String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM User where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<User> getAll(String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM User  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public User save(User obj) {
        String sql = "INSERT INTO User (user_name, user_role,user_password ,user_wage)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getRole());
            stmt.setString(3, obj.getPassword());
            stmt.setDouble(4, obj.getWage());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public User update(User obj) {
        String sql = "UPDATE User"
                + " SET user_name = ?, user_role = ?, user_password = ?, user_wage = ?"
                + " WHERE user_id = ?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getRole());
            stmt.setString(3, obj.getPassword());
            stmt.setDouble(4, obj.getWage());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(User obj) {
        String sql = "DELETE FROM User WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
     public List<EmployeeReport> GetBestEmployee() {
        ArrayList<EmployeeReport> list = new ArrayList();
        String sql = """
                    select user_name AS Name, sum(checkIO_totalHr) AS Total_Hours
                     from User, Checkin_out
                     where User.user_id = Checkin_out.user_id
                     GROUP BY User.user_id
                     ORDER BY Total_Hours DESC
                     LIMIT 1""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                EmployeeReport obj = EmployeeReport.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
