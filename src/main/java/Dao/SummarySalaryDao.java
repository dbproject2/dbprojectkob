package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.SummarySalary;

public class SummarySalaryDao implements Dao<SummarySalary> {

    @Override
    public SummarySalary get(int id) {
        SummarySalary ss = null;
        String sql = "SELECT * FROM SummarySalary WHERE salary_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ss = SummarySalary.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ss;
    }

    @Override
    public List<SummarySalary> getAll() {
        ArrayList<SummarySalary> list = new ArrayList();
        String sql = "SELECT * FROM SummarySalary";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SummarySalary ss = SummarySalary.fromRS(rs);
                list.add(ss);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<SummarySalary> getAllOrderBy(String where, String order) {
        ArrayList<SummarySalary> list = new ArrayList();
        String sql = "SELECT * FROM SummarySalary where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SummarySalary ss = SummarySalary.fromRS(rs);
                list.add(ss);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SummarySalary> getAll(String order) {
        ArrayList<SummarySalary> list = new ArrayList();
        String sql = "SELECT * FROM SummarySalary  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SummarySalary ss = SummarySalary.fromRS(rs);
                list.add(ss);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public SummarySalary save(SummarySalary obj) {
        String sql = "INSERT INTO SummarySalary (salary_id,salary_date,salary_workhr,pay_status)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSalaryId());
            stmt.setDate(2, (Date) obj.getDate());
            stmt.setInt(3, obj.getSalary_hr());
            stmt.setString(4, obj.getStatus());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public SummarySalary update(SummarySalary obj) {
        String sql = "UPDATE SummarySalary"
                + " SET pay_status = ?"
                + " WHERE salary_id = ?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
        
            stmt.setString(1, obj.getStatus());
            stmt.setInt(2, obj.getSalaryId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(SummarySalary obj) {
        String sql = "DELETE FROM SummarySalary WHERE ss_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSalaryId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
 

}
