package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.IncomeReport;
import model.Receipt;
import model.ReceiptDetail;

public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id) {
        Receipt Receipt = null;
        String sql = "SELECT * FROM Receipt WHERE rec_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Receipt = Receipt.fromRS(rs);
                ReceiptDetailDao rdd = new ReceiptDetailDao();
                ArrayList<ReceiptDetail> recieptDetails = (ArrayList<ReceiptDetail>) rdd.getAllOrderBy("rec_id=" + Receipt.getId(), " orderlist_id ");
                Receipt.setReceiptDetails(recieptDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return Receipt;
    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Receipt> getAllOrderBy(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Receipt> getAll(String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {
        String sql = "INSERT INTO Receipt (total_price, received, changed, payment, used_point, recv_point, user_id, cus_id, promo_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setDouble(2, obj.getRecrived());
            stmt.setDouble(3, obj.getChanged());
            stmt.setString(4, obj.getPayment());
            stmt.setInt(5, obj.getUsedPoint());
            stmt.setInt(6, obj.getRecvPoint());
            stmt.setInt(7, obj.getUserId());
            stmt.setInt(8, obj.getCusId());
            stmt.setInt(9, obj.getPromoId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Receipt update(Receipt obj) {
        String sql = "UPDATE Receipt"
                + " SET total_price = ?, received = ?, changed = ?, payment = ?, used_point = ?, recv_point = ?, user_id = ?, cus_id = ?, promo_id = ?"
                + " WHERE rec_id = ?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setDouble(2, obj.getRecrived());
            stmt.setDouble(3, obj.getChanged());
            stmt.setString(4, obj.getPayment());
            stmt.setInt(5, obj.getUsedPoint());
            stmt.setInt(6, obj.getRecvPoint());
            stmt.setInt(7, obj.getUserId());
            stmt.setInt(8, obj.getCusId());
            stmt.setInt(9, obj.getPromoId());
            stmt.setInt(10, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM Receipt WHERE rec_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<IncomeReport> getReceiptIncome(String begin, String end) {
        ArrayList<IncomeReport> list = new ArrayList();
        String sql = """
                    SELECT STRFTIME('%Y-%m-%d',"created_date") AS Mouth, sum(Receipt.total_price) AS Income
                                 FROM Receipt
                                 WHERE STRFTIME('%Y-%m-%d', created_date) BETWEEN ? AND ?
                                 ORDER BY Mouth DESC""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                IncomeReport obj = IncomeReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Receipt> getDateDesc(String begin, String end) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = """
                     SELECT *
                         FROM Receipt
                        WHERE STRFTIME('%Y-%m-%d', created_date) BETWEEN ? AND ?
                        ORDER BY created_date DESC;;""";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Receipt obj = Receipt.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
