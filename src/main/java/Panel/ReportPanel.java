package Panel;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.swing.table.AbstractTableModel;
import model.CostBillReport;
import model.CostEmpReport;
import model.CostRentReport;
import model.CustomerReport;
import model.EmployeeReport;
import model.IncomeReport;
import model.MaterialReport;
import model.ReceiptDetailReport;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import service.CheckInoutService;
import service.CustomerService;
import service.DateLabelFormatter;
import service.MaterialService;
import service.ReceiptService;
import service.RentAndBillsService;
import service.UserService;

public class ReportPanel extends javax.swing.JPanel {

    private ReceiptService recieptService;
    private List<ReceiptDetailReport> recieptDetailList;
    private AbstractTableModel model;
    private final CustomerService customerService;
    private List<CustomerReport> customerList;
    private final UserService userService;
    private List<EmployeeReport> userList;
    private final MaterialService materialService;
    private final List<MaterialReport> materialList;
    private UtilDateModel model1;
    private UtilDateModel model2;
    private DefaultCategoryDataset barDataset;
    private CheckInoutService checkInoutService;
    private RentAndBillsService rentAndBillsService;
    private List<CostBillReport> costbillReport;
    private List<CostEmpReport> costEmpReport;
    private List<CostRentReport> costRentReport;
    private List<IncomeReport> incomeReport;

    public ReportPanel() {
        initComponents();
        recieptService = new ReceiptService();
        recieptDetailList = recieptService.getTopTenProductBySales();
        customerService = new CustomerService();
        customerList = customerService.getTopFiveCustomer();
        userService = new UserService();
        userList = userService.getTheBestEmployee();
        materialService = new MaterialService();
        materialList = materialService.getTopTenProductBySales();
        checkInoutService = new CheckInoutService();
        rentAndBillsService = new RentAndBillsService();

        iniDarePicker();
        initTableProduct();
        initTableCustomer();
        initTableEmployee();
        initTableMaterial();
        initBarChart();

    }

    private void iniDarePicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("txt.today", "Today");
        p1.put("txt.month", "Month");
        p1.put("txt.year", "Year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlDatePicker1.add(datePicker1);
        model1.setSelected(true);

        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("txt.today", "Today");
        p2.put("txt.month", "Month");
        p2.put("txt.year", "Year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlDatePicker2.add(datePicker2);
        model2.setSelected(true);
    }

    private void initTableProduct() {
        model = new AbstractTableModel() {
            String[] columnNames = {"ID", "Name", "Sales", "Qty"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return recieptDetailList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReceiptDetailReport recieptDetail = recieptDetailList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getId();
                    case 1:
                        return recieptDetail.getName();
                    case 2:
                        return recieptDetail.getSales();
                    case 3:
                        return recieptDetail.getQty();
                    default:
                        return "";

                }
            }
        };
        tblProduct.setModel(model);
    }

    private void initTableCustomer() {
        model = new AbstractTableModel() {
            String[] columnNames = {"Name", "Sales"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return customerList.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CustomerReport customer = customerList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return customer.getName();
                    case 1:
                        return customer.getTotalPrice();
                    default:
                        return "";

                }
            }
        };
        tblSpender.setModel(model);
    }

    private void initTableEmployee() {
        model = new AbstractTableModel() {
            String[] columnNames = {"Name", "Total_Hours"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return userList.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                EmployeeReport user = userList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return user.getName();
                    case 1:
                        return user.getTotalHours();
                    default:
                        return "";

                }
            }
        };
        tblEmployee.setModel(model);
    }

    private void initTableMaterial() {
        model = new AbstractTableModel() {
            String[] columnNames = {"Name", "CurrentAmount", "Minimum"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return materialList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                MaterialReport material = materialList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getName();
                    case 1:
                        return material.getMatQty();
                    case 2:
                        return material.getMatMin();
                    default:
                        return "";

                }
            }
        };
        tblMaterial.setModel(model);
    }

    private void initBarChart() {
        barDataset = new DefaultCategoryDataset();
        JFreeChart barChart = ChartFactory.createBarChart(
                "D-coffee Sale",
                "Income",
                "Baht",
                barDataset,
                PlotOrientation.VERTICAL,
                false, true, false);
        int width = 1071 ;
        int height = 306;
        ChartPanel chartPanel1 = new ChartPanel(barChart, width, height, width, height, width, height, true, true, true, true, true, true);

        pnlBarGraph.add(chartPanel1);

    }
    
    private void loadBarDataSet() {
    barDataset.clear();
    SimpleDateFormat monthYearFormat = new SimpleDateFormat("MM-yyyy");

    Map<String, Double> monthlyCosts = new HashMap<>();
    Map<String, Double> monthlyIncome = new HashMap<>();

    for (CostBillReport costBill : costbillReport) {
        String reportMonthYear = monthYearFormat.format(costBill.getDate());
        double cost = costBill.getMatCost();
        monthlyCosts.put(reportMonthYear, monthlyCosts.getOrDefault(reportMonthYear, 0.0) + cost);
    }

    for (CostEmpReport costEmp : costEmpReport) {
        String reportMonthYear = monthYearFormat.format(costEmp.getSalaryDate());
        double cost = costEmp.getEmpSalaryCost();
        monthlyCosts.put(reportMonthYear, monthlyCosts.getOrDefault(reportMonthYear, 0.0) + cost);
    }

    for (CostRentReport costRent : costRentReport) {
        String reportMonthYear = monthYearFormat.format(costRent.getMonth());
        double cost = costRent.getRentCost();
        monthlyCosts.put(reportMonthYear, monthlyCosts.getOrDefault(reportMonthYear, 0.0) + cost);
    }

    for (IncomeReport income : incomeReport) {
        String reportMonthYear = monthYearFormat.format(income.getMonth());
        double incomeValue = income.getIncome();
        monthlyIncome.put(reportMonthYear, monthlyIncome.getOrDefault(reportMonthYear, 0.0) + incomeValue);
    }

    for (Map.Entry<String, Double> entry : monthlyCosts.entrySet()) {
        String[] monthYear = entry.getKey().split("-");
        barDataset.addValue(entry.getValue(), "Combined Cost", monthYear[0] + "-" + monthYear[1]);
        double incomeValue = monthlyIncome.getOrDefault(entry.getKey(), 0.0);
        barDataset.addValue(incomeValue, "Income", monthYear[0] + "-" + monthYear[1]);
    }
}


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pnlBarGraph = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSpender = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblEmployee = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        pnlDatePicker1 = new javax.swing.JPanel();
        pnlDatePicker2 = new javax.swing.JPanel();
        btnProcess = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(1070, 563));
        setPreferredSize(new java.awt.Dimension(1070, 563));

        jPanel2.setBackground(new java.awt.Color(218, 208, 194));

        jLabel1.setFont(new java.awt.Font("PT Serif Caption", 1, 24)); // NOI18N
        jLabel1.setText("Report");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pnlBarGraph.setBackground(new java.awt.Color(86, 86, 86));
        pnlBarGraph.setForeground(new java.awt.Color(255, 255, 255));
        pnlBarGraph.setMaximumSize(new java.awt.Dimension(1071, 306));
        pnlBarGraph.setPreferredSize(null);

        jPanel4.setBackground(new java.awt.Color(100, 137, 127));

        jLabel2.setFont(new java.awt.Font("PT Serif Caption", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Top 10 Product");

        jScrollPane1.setBackground(new java.awt.Color(86, 86, 86));

        tblProduct.setAutoCreateRowSorter(true);
        tblProduct.setBackground(new java.awt.Color(86, 86, 86));
        tblProduct.setForeground(new java.awt.Color(255, 255, 255));
        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblProduct);

        jScrollPane2.setBackground(new java.awt.Color(86, 86, 86));

        tblMaterial.setBackground(new java.awt.Color(86, 86, 86));
        tblMaterial.setForeground(new java.awt.Color(255, 255, 255));
        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblMaterial);

        jLabel3.setFont(new java.awt.Font("PT Serif Caption", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Low in Stock");

        jScrollPane3.setBackground(new java.awt.Color(86, 86, 86));

        tblSpender.setBackground(new java.awt.Color(86, 86, 86));
        tblSpender.setForeground(new java.awt.Color(255, 255, 255));
        tblSpender.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblSpender);

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("PT Serif Caption", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Top 5 Spender");

        jScrollPane4.setBackground(new java.awt.Color(86, 86, 86));

        tblEmployee.setBackground(new java.awt.Color(86, 86, 86));
        tblEmployee.setForeground(new java.awt.Color(255, 255, 255));
        tblEmployee.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tblEmployee);

        jLabel5.setFont(new java.awt.Font("PT Serif Caption", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Best Employee");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(99, 99, 99)
                        .addComponent(jLabel2))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(jLabel3))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(jLabel4)))
                .addGap(51, 51, 51)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(116, 116, 116))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(77, 77, 77))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(0, 23, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(13, 13, 13)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addGap(15, 15, 15))
        );

        jPanel6.setBackground(new java.awt.Color(100, 137, 127));

        btnProcess.setBackground(new java.awt.Color(109, 96, 81));
        btnProcess.setFont(new java.awt.Font("PT Serif Caption", 1, 18)); // NOI18N
        btnProcess.setForeground(new java.awt.Color(255, 255, 255));
        btnProcess.setText("Process");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(pnlDatePicker2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(pnlDatePicker1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlBarGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlBarGraph, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        String patten = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(patten);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        costbillReport = materialService.getCostBill(begin, end);
        costEmpReport = checkInoutService.getCostEmp(begin, end);
        costRentReport = rentAndBillsService.getCostRent(begin, end);
        incomeReport = recieptService.getReceiptIncome(begin, end);
        model.fireTableDataChanged();
        loadBarDataSet();

    }//GEN-LAST:event_btnProcessActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnProcess;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPanel pnlBarGraph;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JTable tblEmployee;
    private javax.swing.JTable tblMaterial;
    private javax.swing.JTable tblProduct;
    private javax.swing.JTable tblSpender;
    // End of variables declaration//GEN-END:variables
}
