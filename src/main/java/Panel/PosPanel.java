package Panel;

import Dialog.CashDialog;
import Dialog.MemberDialog;
import Dialog.PromotionDialog;
import Dialog.PromptpayDialog;
import component.BuyProductable;
import component.ProductListPanel;
import component.UseMember;
import component.UsePromotion;
import java.awt.Frame;

import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import model.Customer;
import model.Product;
import model.Promotion;
import model.Receipt;
import model.ReceiptDetail;
import service.ReceiptService;
import service.UserService;

public class PosPanel extends javax.swing.JPanel implements BuyProductable, UsePromotion, UseMember {

    private ProductListPanel productListPanel;
    private Product product;
    private Receipt receipt;
    private ArrayList<BuyProductable> subscribers = new ArrayList<>();
    private ArrayList<UsePromotion> sub = new ArrayList<>();
    private ArrayList<UseMember> mem = new ArrayList<>();
    private Promotion promotions;
    private PromotionDialog promotionDialog;
    private Promotion editedPromotion;
    private Customer customerMem;
    private MemberDialog memberDialog;
    private PromptpayDialog promptpayDialog;
    private Frame frame;
    private ReceiptService receiptService;
    private CashDialog cashDialog;
    private UserService userService;

    public PosPanel() {
        initComponents();
        setSize(1070, 563);
        receipt = new Receipt();
        receiptService = new ReceiptService();
        promotions = new Promotion();
        customerMem = new Customer();
        productListPanel = new ProductListPanel();
        productListPanel.updateProductsByCategory(1);
        tblShowProducgt.setViewportView(productListPanel);
        productListPanel.AddOnByProduct(this);
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        promotionDialog = new PromotionDialog(frame, promotions);
        promotionDialog.AddOnByPromotion(this);
        memberDialog = new MemberDialog(frame, customerMem);
        memberDialog.AddOnByCustomer(this);
        userService = new UserService();
        receipt.setUser(userService.getCurrentUser());
        
        EnableBtn(false);
        setAllPos();

        tblOrder.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Price", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return receipt.getReceiptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
                ReceiptDetail receiptDetail = receiptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return receiptDetail.getProduct().getName();
                    case 1:
                        return receiptDetail.getProduct().getPrice();
                    case 2:
                        return receiptDetail.getQty();
                    case 3:
                        return receiptDetail.getTotalPrice();
                    default:
                        return "";

                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
                ReceiptDetail receiptDetail = receiptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    receiptDetail.setQty(qty);
                    receipt.calculateTotal();
                    refreshReciept();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });
    }

    private void EnableBtn(boolean b) {
        btnComfirm.setEnabled(b);
        btnCash.setEnabled(b);
        btnPromotion.setEnabled(b);
        btnMember.setEnabled(b);
        btnPromptpay.setEnabled(b);
    }

    private void refreshReciept() {
        tblOrder.revalidate();
        tblOrder.repaint();
        jLabel2.setText("Total: " + receipt.getTotal());
        promptpayDialog = new PromptpayDialog(frame, receipt);
        cashDialog = new CashDialog(frame, receipt);
        EnableBtn(true);

    }

    private void ShowMember(Customer customer) {
        jLabel4.setText("Member Name: " + customer.getName());
        jLabel5.setText("Phone Number: " + customer.getTel());
        jLabel6.setText("Member Point: " + customer.getPoint());
        jLabel7.setText("Point Used: " + receipt.getUsedPoint());
        jLabel8.setText("Member Discount: " + calMemDiscount());
        jLabel9.setText("Total Point: " + (customer.getPoint() - receipt.getUsedPoint()));
        jLabel2.setText("Total: " + (receipt.getTotal() - calMemDiscount()));
        receipt.setTotal(receipt.getTotal() - calMemDiscount());
        refreshReciept();

    }

    private double calMemDiscount() {
        double sum = (receipt.getUsedPoint() * 1) / 10;
        return sum;
    }

    private void ShowPromotion(Promotion promotion) {
        jLabel10.setText("Total: " + receipt.getTotal());
        jLabel11.setText("Discount: " + promotion.getDiscount());
        jLabel12.setText("Total Nat: " + calTotalNat(promotion));
        jLabel2.setText("Total: " + (receipt.getTotal() - promotion.getDiscount()));
        receipt.setTotal(receipt.getTotal() - promotion.getDiscount());
        refreshReciept();

    }

    private double calTotalNat(Promotion promotion) {
        double totalPro = receipt.getTotal() - promotion.getDiscount();
        return totalPro;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tblReciept = new javax.swing.JScrollPane();
        tblOrder = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        btnMember = new javax.swing.JButton();
        btnPromotion = new javax.swing.JButton();
        btnCash = new javax.swing.JButton();
        btnComfirm = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnPromptpay = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        tblShowProducgt = new javax.swing.JScrollPane();
        tblReceipt = new javax.swing.JTable();
        btnDrink = new javax.swing.JButton();
        btnDessert = new javax.swing.JButton();

        setBackground(new java.awt.Color(218, 208, 194));
        setMaximumSize(new java.awt.Dimension(1070, 563));

        jPanel5.setBackground(new java.awt.Color(54, 54, 53));
        jPanel5.setMaximumSize(new java.awt.Dimension(528, 567));

        jPanel7.setBackground(new java.awt.Color(216, 216, 216));
        jPanel7.setMaximumSize(new java.awt.Dimension(60, 93));

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel10.setText("Total :");

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel11.setText("Discont :");

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel12.setText("Total Net :");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12))
                        .addGap(0, 195, Short.MAX_VALUE))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addGap(27, 27, 27))
        );

        jPanel4.setBackground(new java.awt.Color(216, 216, 216));
        jPanel4.setMaximumSize(new java.awt.Dimension(93, 88));

        jLabel13.setBackground(new java.awt.Color(0, 0, 0));
        jLabel13.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel13.setText("Cash :");

        jLabel14.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel14.setText("Change :");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addContainerGap(197, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(216, 216, 216));
        jPanel3.setMaximumSize(new java.awt.Dimension(394, 93));
        jPanel3.setRequestFocusEnabled(false);

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel4.setText("Member Name:");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel5.setText("Phone Number:");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel6.setText("Member Point:");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel7.setText("Point Used:");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel8.setText("Member Discount:");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel9.setText("Total Point:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addGap(139, 139, 139)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(28, 28, 28))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(38, 38, 38)))
                .addGap(46, 46, 46))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(12, 12, 12))
        );

        jLabel2.setFont(new java.awt.Font("Helvetica Neue", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Total :  0");

        tblReciept.setBackground(new java.awt.Color(115, 144, 114));
        tblReciept.setForeground(new java.awt.Color(255, 255, 255));
        tblReciept.setMaximumSize(new java.awt.Dimension(456, 406));

        tblOrder.setBackground(new java.awt.Color(46, 80, 70));
        tblOrder.setForeground(new java.awt.Color(255, 255, 255));
        tblOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblOrder.setMaximumSize(new java.awt.Dimension(300, 80));
        tblReciept.setViewportView(tblOrder);

        jPanel6.setBackground(new java.awt.Color(216, 216, 216));
        jPanel6.setMaximumSize(new java.awt.Dimension(505, 176));

        btnMember.setBackground(new java.awt.Color(46, 80, 70));
        btnMember.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnMember.setForeground(new java.awt.Color(255, 255, 255));
        btnMember.setText("Member");
        btnMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberActionPerformed(evt);
            }
        });

        btnPromotion.setBackground(new java.awt.Color(137, 86, 64));
        btnPromotion.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnPromotion.setForeground(new java.awt.Color(255, 255, 255));
        btnPromotion.setText("Promotion");
        btnPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromotionActionPerformed(evt);
            }
        });

        btnCash.setBackground(new java.awt.Color(63, 148, 122));
        btnCash.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnCash.setForeground(new java.awt.Color(255, 255, 255));
        btnCash.setText("Cash");
        btnCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCashActionPerformed(evt);
            }
        });

        btnComfirm.setBackground(new java.awt.Color(79, 57, 30));
        btnComfirm.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnComfirm.setForeground(new java.awt.Color(255, 255, 255));
        btnComfirm.setText("Confirm");
        btnComfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComfirmActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(153, 51, 0));
        btnCancel.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnPromptpay.setBackground(new java.awt.Color(22, 72, 99));
        btnPromptpay.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        btnPromptpay.setForeground(new java.awt.Color(255, 255, 255));
        btnPromptpay.setText("Promptpay");
        btnPromptpay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromptpayActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnPromotion, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                    .addComponent(btnMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                    .addComponent(btnCash, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnComfirm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPromptpay, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))
                .addGap(16, 16, 16))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCash, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPromptpay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPromotion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnComfirm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tblReciept, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tblReciept, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(56, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(54, 54, 53));
        jPanel1.setMaximumSize(new java.awt.Dimension(564, 567));

        tblShowProducgt.setBackground(new java.awt.Color(100, 137, 127));
        tblShowProducgt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        tblShowProducgt.setForeground(new java.awt.Color(255, 255, 255));
        tblShowProducgt.setMaximumSize(new java.awt.Dimension(552, 500));
        tblShowProducgt.setPreferredSize(new java.awt.Dimension(552, 500));

        tblReceipt.setBackground(new java.awt.Color(46, 80, 70));
        tblReceipt.setForeground(new java.awt.Color(255, 255, 255));
        tblReceipt.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblReceipt.setMaximumSize(new java.awt.Dimension(552, 500));
        tblShowProducgt.setViewportView(tblReceipt);

        btnDrink.setBackground(new java.awt.Color(91, 84, 80));
        btnDrink.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnDrink.setForeground(new java.awt.Color(255, 255, 255));
        btnDrink.setText("Drink");
        btnDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrinkActionPerformed(evt);
            }
        });

        btnDessert.setBackground(new java.awt.Color(91, 84, 80));
        btnDessert.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnDessert.setForeground(new java.awt.Color(255, 255, 255));
        btnDessert.setText("Dessert");
        btnDessert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDessertActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tblShowProducgt, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnDrink, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDessert, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnDrink, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDessert, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tblShowProducgt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnComfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComfirmActionPerformed
        if (receipt.getTotal() >= 100 && receipt.getCusId() != 0) {
            receipt.setRecvPoint(10);
            int cusPoint = customerMem.getPoint() + 10;
            customerMem.setPoint(cusPoint);
        }
        receiptService.addNew(receipt);
        System.out.println("Receipt Finish" + receipt);
        setAllPos();
        receipt = new Receipt();

    }//GEN-LAST:event_btnComfirmActionPerformed

    private void btnDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDrinkActionPerformed
        int categoryID = 1;
        productListPanel.updateProductsByCategory(categoryID);
    }//GEN-LAST:event_btnDrinkActionPerformed

    private void btnPromotionActionPerformed(java.awt.event.ActionEvent evt) {
        promotionDialog.setLocationRelativeTo(null);
        promotionDialog.setVisible(true);
    }

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {
        receipt = new Receipt();
        setAllPos();
        EnableBtn(false);
    }

    private void setAllPos() {
        jLabel2.setText("Total: " + 0);
        jLabel4.setText("Member Name: " + "-");
        jLabel5.setText("Phone Number: " + "-");
        jLabel6.setText("Member Point: " + 0);
        jLabel7.setText("Point Used: " + 0);
        jLabel8.setText("Member Discount: " + 0);
        jLabel9.setText("Total Point: " + 0);
        jLabel10.setText("Total: " + 0);
        jLabel11.setText("Discount: " + 0);
        jLabel12.setText("Total Nat: " + 0);
        jLabel13.setText("Cash: " + 0);
        jLabel14.setText("Change: " + 0);
    }

    private void btnCashActionPerformed(java.awt.event.ActionEvent evt) {
        cashDialog.setLocationRelativeTo(null);
        cashDialog.setVisible(true);
        jLabel13.setText("Cash: " + receipt.getRecrived());
        jLabel14.setText("Change: " + receipt.getChanged());
    }

    private void btnMemberActionPerformed(java.awt.event.ActionEvent evt) {
        memberDialog.setLocationRelativeTo(null);
        memberDialog.setVisible(true);
    }

    private void btnPromptpayActionPerformed(java.awt.event.ActionEvent evt) {
        promptpayDialog.setLocationRelativeTo(null);
        promptpayDialog.setVisible(true);
    }

    private void btnDessertActionPerformed(java.awt.event.ActionEvent evt) {
        int categoryID = 2;
        productListPanel.updateProductsByCategory(categoryID);
    }

    private void btnFoodActionPerformed(java.awt.event.ActionEvent evt) {
        int categoryID = 3;
        productListPanel.updateProductsByCategory(categoryID);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCash;
    private javax.swing.JButton btnComfirm;
    private javax.swing.JButton btnDessert;
    private javax.swing.JButton btnDrink;
    private javax.swing.JButton btnMember;
    private javax.swing.JButton btnPromotion;
    private javax.swing.JButton btnPromptpay;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JTable tblOrder;
    private javax.swing.JTable tblReceipt;
    private javax.swing.JScrollPane tblReciept;
    private javax.swing.JScrollPane tblShowProducgt;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        receipt.addReceiptDetail(product, qty);
        refreshReciept();
    }

    @Override
    public void use(Promotion promotion) {
        receipt.setPromotion(promotion);
        receipt.setPromoId(promotion.getId());
        ShowPromotion(promotion);

    }

    private void dispose() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void member(Customer customer, int usePoint) {
        receipt.setCustomer(customer);
        receipt.setCusId(customer.getId());
        receipt.setUsedPoint(usePoint);
        ShowMember(customer);

    }

}
