package Panel;

import Dialog.PrintmaterialDialog;
import Dialog.AddMaterialDialog;
import Dialog.RestockDialog;
import Dialog.CheckStockDialog;
import Dialog.StockHistoryDialog;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import model.Material;
import model.MaterialBill;
import service.MaterialService;

public class MaterialPanel extends javax.swing.JPanel {

    private MaterialService materialService;
    private List<Material> list;
    private List<Material> selectedMaterials = new ArrayList<>();
    private Material editedMaterial;
    private Material deletedMaterial;
    private String textSearch;
    public List<Material> editedPrintMaterials;
    public List<Material> list2;
    private List<MaterialBill> materialBill;
    private int billId = 1;
    private int hisId = 1;

    public MaterialPanel() {
        initComponents();
        this.deletedMaterial = deletedMaterial;
        InitMaterialTable();
        this.editedMaterial = new Material();
        this.editedPrintMaterials = editedPrintMaterials;
        this.materialBill = new ArrayList<>();
        this.list2 = new ArrayList<>();

    }

    public void InitMaterialTable() {
        if (list2 != null) {
            list2.removeAll(list2);
        }
        materialService = new MaterialService();
        list = materialService.getMaterials();
        tblMaterial.setRowHeight(65);
        tblMaterial.setModel(new AbstractTableModel() {
            String[] columnNames = {"Name", "Quantity", "Price", "Edit", "Delete", "Select"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();

            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 5) {
                    return Boolean.class;
                } else if (columnIndex == 3 || columnIndex == 4) {
                    return ImageIcon.class;
                } else {
                    return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getName();
                    case 1:
                        return material.getQty();
                    case 2:
                        return material.getPricePerUnit();
                    case 3:
                        ImageIcon editIcon = new ImageIcon("./Icon/MatEdit.png");
                        Image editImage = editIcon.getImage();
                        Image newEditImage = editImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
                        editIcon.setImage(newEditImage);
                        return editIcon;
                    case 4:
                        ImageIcon deleteIcon = new ImageIcon("./Icon/MatDelete.png");
                        Image deleteImage = deleteIcon.getImage();
                        Image newDeleteImage = deleteImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
                        deleteIcon.setImage(newDeleteImage);
                        return deleteIcon;
                    case 5:
                        return material.isIsSelected();
                    default:
                        return "UNKNOW";
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex == 5;
            }

            @Override
            public void setValueAt(Object value, int rowIndex, int columnIndex) {
                if (columnIndex == 5 && value instanceof Boolean) {
                    Material material = list.get(rowIndex);
                    material.setIsSelected((Boolean) value);
                    fireTableCellUpdated(rowIndex, columnIndex);
                    System.out.println(material);
                }
            }
        });
        TableColumn selectColumn = tblMaterial.getColumnModel().getColumn(5);
        selectColumn.setCellRenderer(new CheckboxCellRenderer());

    }

    private void InitMaterialTable(String search) {
        if (list2 != null) {
            list2.removeAll(list2);
        }
        materialService = new MaterialService();
        for (Material mat : list) {
            if (mat.getName().equals(search)) {
                list2.add(mat);
            }

        }
        tblMaterial.setRowHeight(65);
        tblMaterial.setModel(new AbstractTableModel() {
            String[] columnNames = {"Name", "Quantity", "Price", "Edit", "Delete", "Select"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list2.size();

            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 5) {
                    return Boolean.class;
                } else if (columnIndex == 3 || columnIndex == 4) {
                    return ImageIcon.class;
                } else {
                    return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = list2.get(rowIndex);
                if (material != null) {
                    switch (columnIndex) {
                        case 0:
                            return material.getName();
                        case 1:
                            return material.getQty();
                        case 2:
                            return material.getPricePerUnit();
                        case 3:
                            ImageIcon editIcon = new ImageIcon("./Icon/MatEdit.png");
                            Image editImage = editIcon.getImage();
                            Image newEditImage = editImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
                            editIcon.setImage(newEditImage);
                            return editIcon;
                        case 4:
                            ImageIcon deleteIcon = new ImageIcon("./Icon/MatDelete.png");
                            Image deleteImage = deleteIcon.getImage();
                            Image newDeleteImage = deleteImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
                            deleteIcon.setImage(newDeleteImage);
                            return deleteIcon;
                        case 5:
                            return material.isIsSelected();
                        default:
                            return "UNKNOW";
                    }
                }
                return "";

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex
            ) {
                return columnIndex == 5;
            }

            @Override
            public void setValueAt(Object value, int rowIndex, int columnIndex) {
                if (columnIndex == 5 && value instanceof Boolean) {
                    Material material = list2.get(rowIndex);
                    material.setIsSelected((Boolean) value);
                    fireTableCellUpdated(rowIndex, columnIndex);
                    System.out.println(material);
                }
            }
        });
        TableColumn selectColumn = tblMaterial.getColumnModel().getColumn(5);
        selectColumn.setCellRenderer(new CheckboxCellRenderer());

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblName1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        btnAddMaterial = new javax.swing.JButton();
        btnRetock = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnSelectAll = new javax.swing.JButton();
        btnCheckMaterial = new javax.swing.JButton();
        btnHistory = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(1070, 563));

        jPanel1.setBackground(new java.awt.Color(218, 208, 194));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        lblName1.setFont(new java.awt.Font("PT Serif Caption", 1, 30)); // NOI18N
        lblName1.setText("Material");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(lblName1, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblName1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(100, 137, 127));

        tblMaterial.setBackground(new java.awt.Color(86, 86, 86));
        tblMaterial.setFont(new java.awt.Font("PT Serif Caption", 0, 14)); // NOI18N
        tblMaterial.setForeground(new java.awt.Color(255, 255, 255));
        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Name", "Quantity", "Price", "Edit", "Delete", "Select"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMaterialMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblMaterial);

        btnAddMaterial.setBackground(new java.awt.Color(116, 125, 101));
        btnAddMaterial.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnAddMaterial.setForeground(new java.awt.Color(255, 255, 255));
        btnAddMaterial.setText("Add Material");
        btnAddMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMaterialActionPerformed(evt);
            }
        });

        btnRetock.setBackground(new java.awt.Color(46, 80, 70));
        btnRetock.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnRetock.setForeground(new java.awt.Color(255, 255, 255));
        btnRetock.setText("Restock");
        btnRetock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRetockActionPerformed(evt);
            }
        });

        btnPrint.setBackground(new java.awt.Color(80, 59, 33));
        btnPrint.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnPrint.setForeground(new java.awt.Color(255, 255, 255));
        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        txtSearch.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        txtSearch.setToolTipText("Search Name");
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });

        btnSearch.setBackground(new java.awt.Color(127, 129, 125));
        btnSearch.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnSelectAll.setBackground(new java.awt.Color(119, 104, 94));
        btnSelectAll.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnSelectAll.setForeground(new java.awt.Color(255, 255, 255));
        btnSelectAll.setText("Select All");
        btnSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectAllActionPerformed(evt);
            }
        });

        btnCheckMaterial.setBackground(new java.awt.Color(137, 128, 98));
        btnCheckMaterial.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnCheckMaterial.setForeground(new java.awt.Color(255, 255, 255));
        btnCheckMaterial.setText("Check Material");
        btnCheckMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckMaterialActionPerformed(evt);
            }
        });

        btnHistory.setBackground(new java.awt.Color(61, 81, 98));
        btnHistory.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnHistory.setForeground(new java.awt.Color(255, 255, 255));
        btnHistory.setText("History");
        btnHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHistoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCheckMaterial)
                        .addGap(28, 28, 28)
                        .addComponent(btnSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 204, Short.MAX_VALUE)
                                .addComponent(btnHistory, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnAddMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnRetock, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16))
                            .addComponent(jScrollPane1))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAddMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnRetock, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnHistory, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                        .addGap(85, 85, 85))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 378, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCheckMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents

    private class CheckboxCellRenderer extends DefaultTableCellRenderer {
        private JCheckBox checkBox = new JCheckBox();
        CheckboxCellRenderer() {
            checkBox.setHorizontalAlignment(JCheckBox.CENTER);
        }
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (value instanceof Boolean) {
                checkBox.setSelected((Boolean) value);
            }
            return checkBox;
        }
    }

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed

    }//GEN-LAST:event_txtSearchActionPerformed


    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        searchMat();
    }//GEN-LAST:event_btnSearchActionPerformed

    public void searchMat() {
        textSearch = txtSearch.getText();
        if (textSearch.isEmpty() || textSearch == null) {
            InitMaterialTable();
        } else {
            InitMaterialTable(textSearch);
        }
        refreshTable();
    }

    private void btnAddMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMaterialActionPerformed
        editedMaterial = new Material();
        openAddDialog();
    }//GEN-LAST:event_btnAddMaterialActionPerformed

    private void tblMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMaterialMouseClicked

        int column = tblMaterial.columnAtPoint(evt.getPoint());
        int row = tblMaterial.rowAtPoint(evt.getPoint());

        if (column == 4) {
            for (Material mat : list2) {
                System.out.println(mat);
            }
            if (tblMaterial.getRowCount() == 1) {
                System.out.println("Okay");
                for (Material mat : list2) {
                    System.out.println(mat);
                    int dialogResult = JOptionPane.showConfirmDialog(this, "Are you sure you want to delete this material?", "Confirm Deletion", JOptionPane.YES_NO_OPTION);
                    if (dialogResult == JOptionPane.YES_OPTION) {
                        materialService.delete(mat);
                        list.remove(mat);
                        refreshTable();

                        InitMaterialTable(null);
                    }
                }
            } else {
                Material materialToDelete = list.get(row);
                int dialogResult = JOptionPane.showConfirmDialog(this, "Are you sure you want to delete this material?", "Confirm Deletion", JOptionPane.YES_NO_OPTION);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    materialService.delete(materialToDelete);
                    list.remove(materialToDelete);
                    refreshTable();
                }
            }

        } else if (column == 3) {
            for (Material mat : list2) {
                System.out.println(mat);
            }
            if (tblMaterial.getRowCount() == 1) {
                System.out.println("Okay");
                for (Material mat : list2) {
                    System.out.println(mat);
                    JFrame frame = (JFrame) SwingUtilities.getRoot(this);
                    AddMaterialDialog materialDialog = new AddMaterialDialog(frame, mat);
                    materialDialog.setObjectToForm();
                    materialDialog.setLocationRelativeTo(this);
                    materialDialog.setVisible(true);
                    materialDialog.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosed(WindowEvent e) {
                            refreshTable();
                        }
                    });
                }
            } else {
                Material materialToEdit = list.get(row);
                JFrame frame = (JFrame) SwingUtilities.getRoot(this);
                AddMaterialDialog materialDialog = new AddMaterialDialog(frame, materialToEdit);
                materialDialog.setObjectToForm();
                materialDialog.setLocationRelativeTo(this);
                materialDialog.setVisible(true);
                materialDialog.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent e) {
                        refreshTable();
                    }
                });
            }
        }
    }//GEN-LAST:event_tblMaterialMouseClicked

    private void btnSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectAllActionPerformed
        materialService = new MaterialService();
        list = materialService.getMaterials();
        selectedMaterials.clear();
        for (Material material : list) {
            System.out.println(material);
            selectedMaterials.add(material);
        }
        openPrintStockDialog();
    }//GEN-LAST:event_btnSelectAllActionPerformed


    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        selectedMaterials.clear();
        for (Material material : list) {
            if (material.isIsSelected()) {
                selectedMaterials.add(material);
            }
        }
        for (Material material : list2) {
            if (material.isIsSelected()) {
                selectedMaterials.add(material);
            }
        }
        if (selectedMaterials.isEmpty()) {
            JOptionPane.showMessageDialog(this, "No materials selected.");
        } else {
            openPrintStockDialog();
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnCheckMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckMaterialActionPerformed
        openCheckStockDialog();
        refreshTable();
    }//GEN-LAST:event_btnCheckMaterialActionPerformed

    private void btnRetockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetockActionPerformed
        openRestockDialog();
    }//GEN-LAST:event_btnRetockActionPerformed

    private void btnHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHistoryActionPerformed
        openHistoryDialog();
    }//GEN-LAST:event_btnHistoryActionPerformed

    private void openHistoryDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        StockHistoryDialog historylDialog = new StockHistoryDialog(frame, materialBill);
        historylDialog.setLocationRelativeTo(this);
        historylDialog.setVisible(true);
        historylDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }

    private void openRestockDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        RestockDialog restockDialog = new RestockDialog(frame, list, materialBill, billId);
        restockDialog.setLocationRelativeTo(this);
        restockDialog.setVisible(true);
        restockDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                ++billId;
                ++hisId;
                refreshTable();
            }
        });

    }

    private void openAddDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        AddMaterialDialog materialDialog = new AddMaterialDialog(frame, editedMaterial);
        materialDialog.setLocationRelativeTo(this);
        materialDialog.setVisible(true);
        materialDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }

    private void openCheckStockDialog() {
        if (editedPrintMaterials == null) {
            return;
        }
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CheckStockDialog checkStockDialog = new CheckStockDialog(frame, editedPrintMaterials, list);
        checkStockDialog.setLocationRelativeTo(this);
        checkStockDialog.setVisible(true);
        checkStockDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
                InitMaterialTable();
            }
        });
    }

    private void openPrintStockDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PrintmaterialDialog checkstockDialog = new PrintmaterialDialog(frame, selectedMaterials);
        checkstockDialog.setLocationRelativeTo(this);
        checkstockDialog.setVisible(true);
        checkstockDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                editedPrintMaterials = checkstockDialog.getEditedPrintMaterials();
                refreshTable();
            }
        });
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 5;
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        if (columnIndex == 5 && value instanceof Boolean) {
            boolean newValue = (Boolean) value;
        }
    }

    private void refreshTable() {
        list = materialService.getMaterials();
        tblMaterial.revalidate();
        tblMaterial.repaint();

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddMaterial;
    private javax.swing.JButton btnCheckMaterial;
    private javax.swing.JButton btnHistory;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnRetock;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnSelectAll;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblName1;
    private javax.swing.JTable tblMaterial;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
