/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package Panel;

import Dialog.ReceiptHistoryDialog;
import Dialog.SelectDateDialog;
import Dialog.SelectDateHistoryDialog;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import service.DateLabelFormatter;
import service.ProductService;
import service.ReceiptService;

/**
 *
 * @author user
 */
public class HistoryReceiptPanel extends javax.swing.JPanel {

    private UtilDateModel model1;
    private UtilDateModel model2;
    private AbstractTableModel model;
    private ReceiptService receiptService;
    private Receipt receipt;
    private List<Receipt> list;
    private JFrame frame;
    private ReceiptHistoryDialog receiptHistoryDialog;
    private ReceiptDetail detail;
    private List<Receipt> receiptDate;
    private SelectDateHistoryDialog selectDateDialog;
    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList<>();

    /**
     * Creates new form HistoryReceiptPanel
     */
    public HistoryReceiptPanel() {
        initComponents();
        iniDarePicker();
        receiptService = new ReceiptService();
        list = receiptService.getReciepts();
        receipt = new Receipt();
        detail = new ReceiptDetail();
        initHistoryReceipt();

    }

    private void iniDarePicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("txt.today", "Today");
        p1.put("txt.month", "Month");
        p1.put("txt.year", "Year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlDatePicker1.add(datePicker1);
        model1.setSelected(true);

        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("txt.today", "Today");
        p2.put("txt.month", "Month");
        p2.put("txt.year", "Year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlDatePicker2.add(datePicker2);
        model2.setSelected(true);
    }

    private void initHistoryReceipt() {
        model = new AbstractTableModel() {
            String[] headers = {"ID", "Date", "Total_Price", "Change", "Payment", "Used_point", "Recv_Point", "User_Id", "Customer_Name", "Promotion_Id", "View"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 11;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Receipt receipt = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return receipt.getId();
                    case 1:
                        return receipt.getCreatedDate();
                    case 2:
                        return receipt.getTotal();
                    case 3:
                        return receipt.getChanged();
                    case 4:
                        return receipt.getPayment();
                    case 5:
                        return receipt.getUsedPoint();
                    case 6:
                        return receipt.getRecvPoint();
                    case 7:
                        return receipt.getUserId();
                    case 8:
                        return receipt.getCusId();
                    case 9:
                        return receipt.getPromoId();
                    case 10:
                        return "Click to Open";
                    default:
                        return "";
                }
            }
        };
        tblHisRe.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int column = tblHisRe.columnAtPoint(e.getPoint());
                int row = tblHisRe.rowAtPoint(e.getPoint());

                if (column == 10 && row >= 0) {
                    openHistoryDialog();
                }
            }
        });
        tblHisRe.setModel(model);
        tblHisRe.setRowHeight(65);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblHisRe = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        pnlDatePicker1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        pnlDatePicker2 = new javax.swing.JPanel();
        btnProcess = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(1067, 512));

        jPanel2.setBackground(new java.awt.Color(218, 208, 194));

        jLabel1.setFont(new java.awt.Font("PT Serif Caption", 1, 24)); // NOI18N
        jLabel1.setText("History Receipt");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(863, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(12, 12, 12))
        );

        jPanel1.setBackground(new java.awt.Color(100, 137, 127));

        tblHisRe.setBackground(new java.awt.Color(86, 86, 86));
        tblHisRe.setForeground(new java.awt.Color(255, 255, 255));
        tblHisRe.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblHisRe);

        jLabel3.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        jLabel3.setText("Start  :");

        jLabel2.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        jLabel2.setText("End :");

        btnProcess.setBackground(new java.awt.Color(109, 96, 81));
        btnProcess.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnProcess.setForeground(new java.awt.Color(255, 255, 255));
        btnProcess.setText("Process");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        btnPrint.setBackground(new java.awt.Color(80, 59, 33));
        btnPrint.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnPrint.setForeground(new java.awt.Color(255, 255, 255));
        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents

    public void updateTableModel(List<Receipt> newRentData) {
        list = newRentData;
        model.fireTableDataChanged();
    }
    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        String patten = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(patten);
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        receiptDate = receiptService.getDate(begin, end);
        updateTableModel(receiptDate);
    }//GEN-LAST:event_btnProcessActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        receipt = new Receipt();
        openDialogSelectDate();
    }//GEN-LAST:event_btnPrintActionPerformed

    private void openHistoryDialog() {
        int selectedRow = tblHisRe.getSelectedRow();
        
        if (selectedRow >= 0) {
            int selectedDate = (int) tblHisRe.getValueAt(selectedRow, 0);
            ArrayList<ReceiptDetail> receiptDetails = new ArrayList<>();
            receiptDetails = (ArrayList<ReceiptDetail>) receiptService.getRecreiptDetailById(selectedDate);
            for (ReceiptDetail receiptDetail : receiptDetails) {
                Product product = new Product();
                ProductService productService = new ProductService();
                product = productService.getById(receiptDetail.getProId());
                receiptDetail.setProduct(product);
                
            }
            this.receiptDetails = receiptDetails;
            receipt.setId(selectedDate);
            openHisReDialog();
            System.out.println(selectedDate);
        }
    }

    private void openDialogSelectDate() {
        frame = (JFrame) SwingUtilities.getRoot(this);
        selectDateDialog = new SelectDateHistoryDialog(frame, receipt);
        selectDateDialog.setLocationRelativeTo(this);
        selectDateDialog.setVisible(true);
        selectDateDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

            private void refreshTable() {
                list = receiptService.getReciepts();
                tblHisRe.revalidate();
                tblHisRe.repaint();
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnProcess;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JTable tblHisRe;
    // End of variables declaration//GEN-END:variables

    private void openHisReDialog() {
        frame = (JFrame) SwingUtilities.getRoot(this);
        receiptHistoryDialog = new ReceiptHistoryDialog(frame, receiptDetails);
        receiptHistoryDialog.setLocationRelativeTo(this);
        receiptHistoryDialog.setVisible(true);
        receiptHistoryDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
            }
        });
    }
}
