package TestService;

import java.time.LocalDateTime;
import model.Promotion;
import service.PromotionService;
import service.ValidatetionException;

public class testPromotionService {

    public static void main(String[] args) throws ValidatetionException {
        LocalDateTime startTime = LocalDateTime.of(2023, 10, 15, 8, 0);
        LocalDateTime endTime = LocalDateTime.of(2023, 10, 31, 8, 0);
        PromotionService pr = new PromotionService();
        Promotion discount200 = new Promotion("discount 200", startTime, endTime, 200.00);
        pr.addNew(discount200);
        Promotion discount500 = new Promotion("discount 500", startTime, endTime, 500.00);
        pr.addNew(discount500);
        for (Promotion addTest : pr.getPromotions()) {
            System.out.println(addTest);
        }
        System.out.println("----------------");

        Promotion del200 = pr.getByName("discount 200");
        pr.delete(del200);
        for (Promotion delTest : pr.getPromotions()) {
            System.out.println(delTest);
        }
        System.out.println("----------------");

        Promotion up500 = pr.getByName("discount 500");
        up500.setName("discount 600");
        up500.setDiscount(600.00);
        pr.update(up500);
        for (Promotion upTest : pr.getPromotions()) {
            System.out.println(upTest);
        }
    }
}
