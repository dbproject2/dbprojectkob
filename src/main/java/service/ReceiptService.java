package service;

import Dao.CheckInOutDao;
import Dao.ReceiptDao;
import Dao.ReceiptDetailDao;
import java.util.List;
import model.IncomeReport;
import model.Receipt;
import model.ReceiptDetail;
import model.ReceiptDetailReport;

public class ReceiptService {

    public Receipt getById(int id) {
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.get(id);
    }


    public List<Receipt> getReciepts() {
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.getAll(" rec_id desc");
    }

    public Receipt addNew(Receipt editedReciept) {
        ReceiptDao recieptDao = new ReceiptDao();
        ReceiptDetailDao recieptDetailDao = new ReceiptDetailDao();
        Receipt receipt = new Receipt();
        Receipt reciept = recieptDao.save(editedReciept);
        for (ReceiptDetail rd : editedReciept.getReceiptDetails()) {
            rd.setRecId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public Receipt update(Receipt editedReciept) {
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(Receipt editedReciept) {
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.delete(editedReciept);
    }

    public List<ReceiptDetailReport> getTopTenProductBySales() {
        ReceiptDetailDao recieptDao = new ReceiptDetailDao();
        return recieptDao.GetTopTenProductSales();
    }

    public List<IncomeReport> getReceiptIncome(String begin, String end) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getReceiptIncome(begin, end);
    }
     public List<ReceiptDetail> getRecreiptDetailById(int id) {
        ReceiptDetailDao receiptDetail = new ReceiptDetailDao();
        return receiptDetail.getAllByRecId(id);
    }
       public List<Receipt> getDate(String begin, String end) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getDateDesc(begin, end);
    }


}
