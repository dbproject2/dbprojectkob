package service;

import Dao.SalaryRecordDao;
import java.util.List;
import model.SalaryRecord;

public class SalaryRecordService {

    private SalaryRecordDao pd = new SalaryRecordDao();

    public SalaryRecord getById(int id) {
        SalaryRecordDao salaryRecordDao = new SalaryRecordDao();
        SalaryRecord salaryRecord = salaryRecordDao.get(id);
        return salaryRecord;
    }

    public SalaryRecord addNew(SalaryRecord editedSalaryRecord) {
        SalaryRecordDao pd = new SalaryRecordDao();
        return pd.save(editedSalaryRecord);
    }

    public SalaryRecord update(SalaryRecord editedSalaryRecord) {
        SalaryRecordDao pd = new SalaryRecordDao();
        return pd.update(editedSalaryRecord);
    }

    public int delete(SalaryRecord editedSalaryRecord) {
        SalaryRecordDao pd = new SalaryRecordDao();
        return pd.delete(editedSalaryRecord);
    }

    public List<SalaryRecord> getRecordsById() {
        SalaryRecordDao salaryRecordDao = new SalaryRecordDao();
        return salaryRecordDao.getAll(" record_id asc");
    }

    public List<SalaryRecord> getRecords() {
        SalaryRecordDao salaryRecordDao = new SalaryRecordDao();
        return salaryRecordDao.getAll();
    }

    public List<SalaryRecord> getRecordsByDate(String Date) {
        SalaryRecordDao salaryRecordDao = new SalaryRecordDao();
        return salaryRecordDao.getAll();
    }
}
