package service;

import Dao.RentAndBillsDao;
import java.util.List;
import model.CostRentReport;
import model.RentAndBills;

public class RentAndBillsService {

    public RentAndBills getById(int id) {
        RentAndBillsDao rentAndBillsDao = new RentAndBillsDao();
        RentAndBills rentAndBills = rentAndBillsDao.get(id);
        return rentAndBills;
    }

    public List<RentAndBills> getRentAndBillsList() {
        RentAndBillsDao rentAndBillsDao = new RentAndBillsDao();
        return rentAndBillsDao.getAll(" created_date desc");
    }

    public RentAndBills addNew(RentAndBills editedRentAndBills) {
        RentAndBillsDao rentAndBillsDao = new RentAndBillsDao();
        return rentAndBillsDao.save(editedRentAndBills);
    }

    public RentAndBills update(RentAndBills editedRentAndBills) {
        RentAndBillsDao rentAndBillsDao = new RentAndBillsDao();
        return rentAndBillsDao.update(editedRentAndBills);
    }

    public int delete(RentAndBills editedRentAndBills) {
        RentAndBillsDao rentAndBillsDao = new RentAndBillsDao();
        return rentAndBillsDao.delete(editedRentAndBills);
    }

    public List<CostRentReport> getCostRent(String begin, String end) {
        RentAndBillsDao rentAndBillsDao = new RentAndBillsDao();
        return rentAndBillsDao.getCostRent(begin, end);
    }
    
     public List<RentAndBills> getDate(String begin, String end) {
        RentAndBillsDao rentAndBillsDao = new RentAndBillsDao();
        return rentAndBillsDao.getDateDesc(begin, end);
    }

}
