package service;

import Dao.CategoryDao;
import java.util.List;
import model.Category;

public class CategoryService {

    public List<Category> getCategorys() {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll("cat_id ASC");
    }

    public Category getByName(String name) {
        CategoryDao categoryDao = new CategoryDao();
        Category category = categoryDao.getByName(name);
        return category;
    }

      public Category getNameById(int id) {
        CategoryDao categoryDao = new CategoryDao();
        Category category = categoryDao.get(id);
        return category;
    }
    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    }

    public Category getName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
