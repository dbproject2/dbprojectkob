package service;

import Dao.CheckInOutDao;
import java.util.Date;
import java.util.List;
import model.CheckInOut;
import model.CostEmpReport;

public class CheckInoutService {

    private CheckInOutDao pd = new CheckInOutDao();

    public CheckInOut getById(int id) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        CheckInOut checkInOut = checkInOutDao.get(id);
        return checkInOut;
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao pd = new CheckInOutDao();
        return pd.save(editedCheckInOut);
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        CheckInOutDao pd = new CheckInOutDao();
        return pd.update(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        CheckInOutDao pd = new CheckInOutDao();
        return pd.delete(editedCheckInOut);
    }

    public int deleteAllById(int id) {
        CheckInOutDao pd = new CheckInOutDao();
        return pd.deleteAllById(id);
    }

    public List<CheckInOut> getCheckInOuts() {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAll(" checkIO_id asc");
    }

    public List<CheckInOut> getCheckInOutsById(int id) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAllByUserId(id);
    }

    public List<CostEmpReport> getCostEmp(String begin, String end) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getCostEmp(begin, end);
    }

    public List<CheckInOut> getAllByUserId(int userId) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAllByUserId(userId);
    }

    public boolean hasUserLoggedOnDay(int userId, Date date) {
        List<CheckInOut> checkInOuts = getAllByDate(date);
        for (CheckInOut checkInOut : checkInOuts) {
            if (checkInOut.getUserId() == userId) {
                return true;
            }
        }
        return false;
    }

    private List<CheckInOut> getAllByDate(Date date) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAllByDate(date);
    }

}
