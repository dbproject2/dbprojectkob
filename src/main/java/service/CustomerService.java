package service;

import Dao.CustomerDao;
import java.util.List;
import model.Customer;
import model.CustomerReport;

public class CustomerService {

    public Customer getByTel(String tel) {
        CustomerDao customerDao = new CustomerDao();
        Customer customer = customerDao.getByTel(tel);
        return customer;
    }

    public List<Customer> getCustomers() {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll(" cus_id asc");
    }

    public Customer addNew(Customer editedCustomer) throws ValidatetionException {
        if (!editedCustomer.isValid()) {
            throw new ValidatetionException("Customer is invalid!!");
        }

        CustomerDao customerDao = new CustomerDao();
        return customerDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) throws ValidatetionException {
        if (!editedCustomer.isValid()) {
            throw new ValidatetionException("Customer is invalid!!");
        }
        CustomerDao customerDao = new CustomerDao();
        return customerDao.update(editedCustomer);
    }

    public int delete(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.delete(editedCustomer);
    }

    public List<CustomerReport> getTopFiveCustomer() {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.GetTopFiveSpender();
    }

}
