package service;

import Dao.UserDao;
import java.util.List;
import model.EmployeeReport;
import model.User;

public class UserService {

    public static User currentUser;

    public static User login(String name, char[] password) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if (user != null && user.getPassword().equals(password)) {
            currentUser = user;
            return user;
        }
        return null;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public User getByName(String name) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        return user;

    }

    public User getNameById(int id) {
        UserDao userDao = new UserDao();
        User user = userDao.get(id);
        return user;

    }

    public List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_id asc");
    }

     public User addNew(User editedUser) throws ValidatetionException {
        if (!editedUser.isValid()) {
            throw new ValidatetionException("User is invalid!!");
        }

        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }

    public User update(User editedUser) throws ValidatetionException {
        if (!editedUser.isValid()) {
            throw new ValidatetionException("User is invalid!!");
        }
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }

    public int delete(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editedUser);
    }

    public List<EmployeeReport> getTheBestEmployee() {
        UserDao userDao = new UserDao();
        return userDao.GetBestEmployee();
    }
}
