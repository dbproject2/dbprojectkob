package service;

import Dao.MaterialDao;
import java.util.List;
import model.CostBillReport;
import model.Material;
import model.MaterialReport;

public class MaterialService {

    public Material getByName(String name) {
        MaterialDao materialDao = new MaterialDao();
        Material material = materialDao.getByName(name);
        return material;
    }

    public Material getById(int id) {
        MaterialDao materialDao = new MaterialDao();
        Material material = materialDao.get(id);
        return material;
    }

    public List<Material> getMaterials() {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getAll(" mat_id asc");
    }

    public Material addNew(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }

    public List<MaterialReport> getTopTenProductBySales() {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.GetLowInStock();
    }

    public List<CostBillReport> getCostBill(String begin, String end) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getCostBill(begin, end);
    }
}
