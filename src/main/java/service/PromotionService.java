package service;

import Dao.PromotionDao;
import java.util.List;
import model.Promotion;

public class PromotionService {

    public List<Promotion> getPromotions() {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(" promo_id asc");
    }

    public Promotion getByName(String name) {
        PromotionDao promotionDao = new PromotionDao();
        Promotion promotion = promotionDao.getByName(name);
        return promotion;
    }

    public Promotion addNew(Promotion editedPromotion) throws ValidatetionException {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) throws ValidatetionException {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }
}
