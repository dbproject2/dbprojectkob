package service;

import Dao.SummarySalaryDao;
import java.util.List;
import model.SummarySalary;

public class SummarySalaryService {

    public List<SummarySalary> getSalarys() {
        SummarySalaryDao salaryDao = new SummarySalaryDao();
        return salaryDao.getAll(" salary_id asc");
    }

    public SummarySalary getSalaryById(int id){
        SummarySalaryDao salaryDao = new SummarySalaryDao();
        return salaryDao.get(id);
    }
    public SummarySalary addNew(SummarySalary editedSummarySalary) {
        SummarySalaryDao salaryDao = new SummarySalaryDao();
        return salaryDao.save(editedSummarySalary);
    }

    public SummarySalary update(SummarySalary editedSummarySalary) {
        SummarySalaryDao salaryDao = new SummarySalaryDao();
        return salaryDao.update(editedSummarySalary);
    }

    public int delete(SummarySalary editedSummarySalary) {
        SummarySalaryDao salaryDao = new SummarySalaryDao();
        return salaryDao.delete(editedSummarySalary);
    }
}
