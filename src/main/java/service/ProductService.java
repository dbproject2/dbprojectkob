package service;

import Dao.ProductDao;
import java.util.ArrayList;
import java.util.List;
import model.Product;

public class ProductService {

    private ProductDao pd = new ProductDao();

    public List<Product> getByCatId(int id) {
        ProductDao pd = new ProductDao();
        return (ArrayList<Product>) pd.getByCatId(id);
    }

    public List<Product> getProducts() {
        ProductDao pd = new ProductDao();
        return (ArrayList<Product>) pd.getAll("pro_name DESC");
    }

    public Product getByName(String name) {
        ProductDao pd = new ProductDao();
        return pd.getByName(name);
    }

    public Product getById(int id) {
        ProductDao pd = new ProductDao();
        return pd.get(id);
    }

    public Product addNew(Product editedProduct) {
        ProductDao pd = new ProductDao();
        return pd.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao pd = new ProductDao();
        return pd.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao pd = new ProductDao();
        return pd.delete(editedProduct);
    }

}
