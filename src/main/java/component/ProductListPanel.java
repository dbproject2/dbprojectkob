package component;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import model.Product;
import service.ProductService;

public class ProductListPanel extends javax.swing.JPanel implements BuyProductable {

    private ProductService productService;
    private ArrayList<Product> products;
    private ArrayList<BuyProductable> subscribers;
    private ProductItemPanel pnlProductListItem;

    public ProductListPanel() {
        initComponents();
        productService = new ProductService();
        subscribers = new ArrayList<BuyProductable>();
        products = (ArrayList<Product>) productService.getProducts();
        int productSize = products.size();
    }

    public void AddOnByProduct(BuyProductable subsciber) {
        subscribers.add(subsciber);
    }

    public void updateProductsByCategory(int categoryId) {
        pnlProductList.removeAll();
        List<Product> productsToDisplay = new ArrayList<>();

        for (Product p : products) {
            if (p.getCatId() == categoryId) {
                productsToDisplay.add(p);
            }
        }

        int rows = (int) Math.ceil(productsToDisplay.size() / 3.0);
        pnlProductList.setLayout(new GridLayout(rows, 3, 0, 0));

        for (Product p : productsToDisplay) {
            pnlProductListItem = new ProductItemPanel(p);
            pnlProductListItem.AddOnByProduct(this);
            pnlProductList.add(pnlProductListItem);
        }

        pnlProductList.revalidate();
        pnlProductList.repaint();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlProductList = new javax.swing.JPanel();

        pnlProductList.setBackground(new java.awt.Color(46, 80, 70));
        pnlProductList.setMaximumSize(new java.awt.Dimension(552, 500));

        javax.swing.GroupLayout pnlProductListLayout = new javax.swing.GroupLayout(pnlProductList);
        pnlProductList.setLayout(pnlProductListLayout);
        pnlProductListLayout.setHorizontalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 552, Short.MAX_VALUE)
        );
        pnlProductListLayout.setVerticalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlProductList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlProductList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel pnlProductList;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        for (BuyProductable s : subscribers) {
            s.buy(product, qty);
        }
    }

}
