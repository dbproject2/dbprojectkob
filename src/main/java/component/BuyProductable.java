package component;

import model.Product;

public interface BuyProductable {
    public void buy(Product product, int qty);

}
