package component;

import model.Promotion;

public interface UsePromotion {
    public void use(Promotion promotion);

}
