package component;

import Dialog.AddtocartDialog;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import model.Product;

public class ProductItemPanel extends javax.swing.JPanel implements BuyProductable {

    private Product products;
    private ArrayList<BuyProductable> subscribers;
    private final AddtocartDialog addtocartDialog;
    private Product editedProduct;

    public ProductItemPanel(Product p) {
        initComponents();
        products = p;
        lblName.setText(products.getName());
        subscribers = new ArrayList<>();
        ImageIcon icon = new ImageIcon("./pictures/product" + products.getId() + ".png");
        Image image = icon.getImage();
        Image newImage = image.getScaledInstance(185, 160, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);

        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        addtocartDialog = new AddtocartDialog(frame, products.getId());
        addtocartDialog.AddOnByProduct(this);

    }

    public void AddOnByProduct(BuyProductable subsciber) {
        subscribers.add(subsciber);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblImage = new javax.swing.JLabel();
        btnAddcart = new javax.swing.JButton();
        lblName = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(46, 80, 70));

        lblImage.setBackground(new java.awt.Color(218, 208, 194));
        lblImage.setOpaque(true);
        lblImage.setVerifyInputWhenFocusTarget(false);

        btnAddcart.setBackground(new java.awt.Color(81, 142, 124));
        btnAddcart.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        btnAddcart.setForeground(new java.awt.Color(255, 255, 255));
        btnAddcart.setText("Add to Cart");
        btnAddcart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddcartActionPerformed(evt);
            }
        });

        lblName.setBackground(new java.awt.Color(58, 77, 57));
        lblName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblName.setForeground(new java.awt.Color(255, 255, 255));
        lblName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAddcart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(26, 26, 26))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                .addGap(12, 12, 12)
                .addComponent(btnAddcart, javax.swing.GroupLayout.PREFERRED_SIZE, 40, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddcartActionPerformed(java.awt.event.ActionEvent evt) {
        openDialog();
    }

    private void openDialog() {
        addtocartDialog.setLocationRelativeTo(this);
        addtocartDialog.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddcart;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblName;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        for (BuyProductable s : subscribers) {
            s.buy(product, qty);
        }
    }

}
