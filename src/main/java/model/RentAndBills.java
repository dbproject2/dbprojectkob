package model;

import Dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RentAndBills {

    private int id;
    private Date createdDate;
    private double rentCost;
    private double electricCost;
    private double waterCost;
    private int userId;
    private User user;

    public RentAndBills(int id, Date createdDate, double rentCost, double electricCost, double waterCost, int userId) {
        this.id = id;
        this.createdDate = createdDate;
        this.rentCost = rentCost;
        this.electricCost = electricCost;
        this.waterCost = waterCost;
        this.userId = userId;
    }

    public RentAndBills(Date createdDate, double rentCost, double electricCost, double waterCost, int userId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.rentCost = rentCost;
        this.electricCost = electricCost;
        this.waterCost = waterCost;
        this.userId = userId;
    }

    public RentAndBills(double rentCost, double electricCost, double waterCost, int userId) {
        this.id = -1;
        this.createdDate = null;
        this.rentCost = rentCost;
        this.electricCost = electricCost;
        this.waterCost = waterCost;
        this.userId = userId;
    }

    public RentAndBills() {
        this.id = -1;
        this.createdDate = null;
        this.rentCost = 0;
        this.electricCost = 0;
        this.waterCost = 0;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public double getRentCost() {
        return rentCost;
    }

    public void setRentCost(double rentCost) {
        this.rentCost = rentCost;
    }

    public double getElectricCost() {
        return electricCost;
    }

    public void setElectricCost(double electricCost) {
        this.electricCost = electricCost;
    }

    public double getWaterCost() {
        return waterCost;
    }

    public void setWaterCost(double waterCost) {
        this.waterCost = waterCost;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "RentAndBills{" + "id=" + id + ", createdDate=" + createdDate + ", rentCost=" + rentCost + ", electricCost=" + electricCost + ", waterCost=" + waterCost + ", userId=" + userId + ", user=" + user + '}';
    }


    public static RentAndBills fromRS(ResultSet rs) {
        RentAndBills rentAndBills = new RentAndBills();
        try {
            rentAndBills.setId(rs.getInt("id"));
            rentAndBills.setCreatedDate(rs.getTimestamp("created_date"));
            rentAndBills.setRentCost(rs.getDouble("rent_cost"));
            rentAndBills.setElectricCost(rs.getDouble("electric_cost"));
            rentAndBills.setWaterCost(rs.getDouble("water_cost"));
            rentAndBills.setUserId(rs.getInt("user_id"));
            UserDao userDao = new UserDao();
            User user = userDao.get(rentAndBills.getUserId());
            rentAndBills.setUser(user);

        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return rentAndBills;

    }

}
