package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class User {

    private int id;
    private String name;
    private String role;
    private String password;
    private double wage;

    public User(int id, String name, String role, String password, double wage) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.password = password;
        this.wage = wage;

    }

    public User(String name, String role, String password, double wage) {
        this.id = -1;
        this.name = name;
        this.role = role;
        this.password = password;
        this.wage = wage;

    }

    public User() {
        this.id = -1;
        this.name = "";
        this.role = "USER";
        this.password = "";
        this.wage = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", role=" + role + ", password=" + password + ", wage=" + wage + '}';
    }

    public static User fromRS(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getInt("user_id"));
            user.setName(rs.getString("user_name"));
            user.setRole(rs.getString("user_role"));
            user.setPassword(rs.getString("user_password"));
            user.setWage(rs.getDouble("user_wage"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }
    public boolean isValid() {
        return this.name.length() >= 4
                && this.password.length() >= 5
                && this.wage >= 0;
    }

}
