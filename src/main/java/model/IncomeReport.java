package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IncomeReport {

    private Date month;
    private double income;

    public IncomeReport(Date month, double income) {
        this.month = month;
        this.income = income;
    }

    public IncomeReport() {
        this(null, 0);

    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    @Override
    public String toString() {
        return "IncomeReport{" + "month=" + month + ", income=" + income + '}';
    }

   public static IncomeReport fromRS(ResultSet rs) {
    IncomeReport incomeReport = new IncomeReport();
    try {
        String dateString = rs.getString("Mouth");
        if (dateString != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(dateString);
            incomeReport.setMonth(date);
        }
        incomeReport.setIncome(rs.getDouble("Income"));
    } catch (SQLException | ParseException ex) {
        Logger.getLogger(IncomeReport.class.getName()).log(Level.SEVERE, null, ex);
        return null;
    }
    return incomeReport;
}


}
