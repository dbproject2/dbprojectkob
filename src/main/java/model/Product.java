package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Product {

    private int id;
    private String name;
    private float price;
    private String size;
    private String sweetLevel;
    private String type;
    private int catId;

    public Product(int id, String name, float price, String size, String sweetLevel, String type, int catId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.size = size;
        this.sweetLevel = sweetLevel;
        this.type = type;
        this.catId = catId;
    }

    public Product(String name, float price, String size, String sweetLevel, String type, int catId) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.size = size;
        this.sweetLevel = sweetLevel;
        this.type = type;
        this.catId = catId;
    }

    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.size = "";
        this.sweetLevel = "";
        this.type = "";
        this.catId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float calculatePrice(float price, float add) {
        float sum = price + add;
        return sum;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSweetLevel() {
        return sweetLevel;
    }

    public void setSweetLevel(String sweetLevel) {
        this.sweetLevel = sweetLevel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", size=" + size + ", sweetLevel=" + sweetLevel + ", type=" + type + ", catId=" + catId + '}';
    }

    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("pro_id"));
            product.setName(rs.getString("pro_name"));
            product.setPrice(rs.getFloat("pro_price"));
            product.setSize(rs.getString("pro_size"));
            product.setSweetLevel(rs.getString("pro_sweetlv"));
            product.setType(rs.getString("pro_type"));
            product.setCatId(rs.getInt("cat_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }

}
