package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CostEmpReport {

    private Date salaryDate;
    private double empSalaryCost;

    public CostEmpReport(Date salaryDate, double empSalaryCost) {
        this.salaryDate = salaryDate;
        this.empSalaryCost = empSalaryCost;
    }

    public CostEmpReport() {
        this(null, 0);
    }

    public Date getSalaryDate() {
        return salaryDate;
    }

    public void setSalaryDate(Date salaryDate) {
        this.salaryDate = salaryDate;
    }

    public double getEmpSalaryCost() {
        return empSalaryCost;
    }

    public void setEmpSalaryCost(double empSalaryCost) {
        this.empSalaryCost = empSalaryCost;
    }

    @Override
    public String toString() {
        return "CostEmpReport{" + "salaryDate=" + salaryDate + ", empSalaryCost=" + empSalaryCost + '}';
    }

    public static CostEmpReport fromRS(ResultSet rs) {
        CostEmpReport costempReport = new CostEmpReport();
        try {
            String dateString = rs.getString("Month");
            if (dateString != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = dateFormat.parse(dateString);
                costempReport.setSalaryDate(new Timestamp(date.getTime()));
            }
            costempReport.setEmpSalaryCost(rs.getDouble("Employee_Salary_Cost"));
        } catch (SQLException | ParseException ex) {
            Logger.getLogger(CostEmpReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return costempReport;
    }

}
