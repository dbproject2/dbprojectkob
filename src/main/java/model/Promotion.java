package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Promotion {

    private int id;
    private String name;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private double discount;
    private ArrayList<Promotion> promotions = new ArrayList<>();
    private Promotion promo;

    public Promotion(int id, String name, LocalDateTime startTime, LocalDateTime endTime, double discount) {
        this.id = id;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.discount = discount;
    }

    public Promotion(String name, LocalDateTime startTime, LocalDateTime endTime, double discount) {
        this.id = -1;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.discount = discount;
    }

    public Promotion(String discount_200, String string, String string1, double par) {
        this.id = -1;
        this.name = "";
        this.startTime = LocalDateTime.of(1970, 1, 1, 0, 0);
        this.endTime = LocalDateTime.of(1970, 1, 1, 0, 0);
        this.discount = 0.00;
    }
     public Promotion() {
        this.id = -1;
        this.name = "";
        this.startTime = null;
        this.endTime = null;
        this.discount = 0.00;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
    public void AddPromotion(Promotion promotion) {
        promo = new Promotion();
        promo.setDiscount(promotion.getDiscount());
        promotions.add(promo);
    }
    
    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", startTime=" + startTime + ", endTime=" + endTime + ", discount=" + discount + '}';
    }

    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion("discount 200", "15/10/2023", "31/10/2023", 200.0);
        try {
            promotion.setId(rs.getInt("promo_id"));
            promotion.setName(rs.getString("promo_name"));
            promotion.setStartTime(rs.getTimestamp("promo_starttime").toLocalDateTime());
            promotion.setEndTime(rs.getTimestamp("promo_endtime").toLocalDateTime());
            promotion.setDiscount(rs.getDouble("promo_discount"));
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;

    }

    public boolean isValid() {
        return this.name.length() >= 3
                && this.discount <= 10000.00;
    }

}
