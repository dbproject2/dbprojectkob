package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MaterialReport {
    private String name;
    private int matQty;
    private int matMin;

    public MaterialReport(String name, int matQty, int matMin) {
        this.name = name;
        this.matQty = matQty;
        this.matMin = matMin;
    }

    public MaterialReport() {
        this("", 0, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMatQty() {
        return matQty;
    }

    public void setMatQty(int matQty) {
        this.matQty = matQty;
    }

    public int getMatMin() {
        return matMin;
    }

    public void setMatMin(int matMin) {
        this.matMin = matMin;
    }

    @Override
    public String toString() {
        return "MaterialReport{" + "name=" + name + ", matQty=" + matQty + ", matMin=" + matMin + '}';
    }
     public static MaterialReport fromRS(ResultSet rs) {
        MaterialReport materialReport = new MaterialReport();
        try {
            materialReport.setName(rs.getString("Name"));
            materialReport.setMatQty(rs.getInt("CurrentAmount"));
            materialReport.setMatMin(rs.getInt("Minimum"));
        } catch (SQLException ex) {
            Logger.getLogger(MaterialReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return materialReport;
    }
}
