package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomerReport {
    private String name;
    private double totalPrice;

    public CustomerReport(String name, double totalPrice) {
        this.name = name;
        this.totalPrice = totalPrice;
    }

    public CustomerReport() {
        this("", 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "CustomerReport{" + "name=" + name + ", totalPrice=" + totalPrice + '}';
    }

    public static CustomerReport fromRS(ResultSet rs) {
        CustomerReport customerReport = new CustomerReport();
        try {
            customerReport.setName(rs.getString("Name"));
            customerReport.setTotalPrice(rs.getInt("Sales"));
        } catch (SQLException ex) {
            Logger.getLogger(CustomerReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customerReport;
    }

}
