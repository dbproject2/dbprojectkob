package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Material {

    private int id;
    private String name;
    private int revDate;
    private float qty;
    private float min;
    private String unit;
    private int pricePerUnit;
    private boolean isSelected ;
    private boolean correct;

    public Material(int id, String name, int revDate, float qty, float min, String unit, int pricePerUnit, boolean isSelected) {
        this.id = id;
        this.name = name;
        this.revDate = revDate;
        this.qty = qty;
        this.min = min;
        this.unit = unit;
        this.pricePerUnit = pricePerUnit;
        this.isSelected = isSelected;
        this.correct = false; 
    }

    public Material(String name, int revDate, float qty, float min, String unit, int pricePerUnit, boolean isSelected) {
        this.id = -1;
        this.name = name;
        this.revDate = revDate;
        this.qty = qty;
        this.min = min;
        this.unit = unit;
        this.pricePerUnit = pricePerUnit;
        this.isSelected = isSelected;
        this.correct = false; 
    }

    public Material() {
        this.id = -1;
        this.name = "";
        this.revDate = 0;
        this.qty = 0.0f;
        this.min = 0.0f;
        this.unit = "";
        this.pricePerUnit = 0;
        this.isSelected = isSelected;
        this.correct = false; 
    }

    public Material(String name, float quantity) {
        this.id = -1;
        this.name = name;
        this.revDate = 0;
        this.qty = quantity;
        this.min = 0.0f;
        this.unit = "";
        this.pricePerUnit = 0;
        this.isSelected = isSelected;
        this.correct = false; 
    }
    
    

    public boolean isIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRevDate() {
        return revDate;
    }

    public void setRevDate(int revDate) {
        this.revDate = revDate;
    }

    public float getQty() {
        return qty;
    }

    public void setQty(float qty) {
        this.qty = qty;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(int pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", revDate=" + revDate + ",qty=" + qty + ", min=" + min + ", unit=" + unit + ", pricePerUnit=" + pricePerUnit + "selected=" + isSelected + '}';
    }

    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("mat_id"));
            material.setName(rs.getString("mat_name"));
            material.setRevDate(rs.getInt("mat_revdate"));
            material.setQty(rs.getFloat("mat_qty"));
            material.setMin(rs.getFloat("mat_min"));
            material.setUnit(rs.getString("mat_unit"));
            material.setPricePerUnit(rs.getInt("mat_price_per_unit"));

        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }

    public void setCorrect(Boolean correct) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

   

}
