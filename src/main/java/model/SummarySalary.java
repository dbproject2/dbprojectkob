package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SummarySalary {

    private int salaryId;
    private Date date;
    private int salary_hr;
    private String status;

    public SummarySalary(int salaryId, Date date, int salary_hr, String status) {
        this.salaryId = salaryId;
        this.date = date;
        this.salary_hr = salary_hr;
        this.status = status;
    }


    public SummarySalary(int salaryId,int salary_hr, String status) {
        this.salaryId = salaryId;
        this.date = null;
        this.salary_hr = salary_hr;
        this.status = status;
    }
    public SummarySalary() {
        this.salaryId = -1;
        this.date = null;
        this.salary_hr = 0;
        this.status = "";
    }

    public int getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(int salaryId) {
        this.salaryId = salaryId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getSalary_hr() {
        return salary_hr;
    }

    public void setSalary_hr(int salary_hr) {
        this.salary_hr = salary_hr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SummarySalary{" + "salaryId=" + salaryId + ", date=" + date + ", salary_hr=" + salary_hr + ", status=" + status + '}';
    }
    
       public static SummarySalary fromRS(ResultSet rs) {
        SummarySalary ss = new SummarySalary();
        try {
            ss.setSalaryId(rs.getInt("salary_id"));
            ss.setDate(rs.getDate("salary_date"));
            ss.setSalary_hr(rs.getInt("salary_workhr"));
            ss.setStatus(rs.getString("pay_status"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ss;
    }

}
