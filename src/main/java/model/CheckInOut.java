package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CheckInOut {

    private int id;
    private Date date, timeIn, timeOut;
    private int totalHour;
    private int userId;
    private int salaryId;

    public CheckInOut(int id, Date date, Date timeIn, Date timeOut, int totalHour, int userId, int salaryId) {
        this.id = id;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.totalHour = totalHour;
        this.userId = userId;
        this.salaryId = salaryId;
    }

    public CheckInOut(int userId, int salaryId) {
        this.id = -1;
        this.userId = userId;
        this.salaryId = salaryId;
    }

    public CheckInOut() {
        this.id = -1;
        this.date = null;
        this.timeIn = null;
        this.timeOut = null;
        this.totalHour = 0;
        this.userId = -1;
        this.salaryId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(Date timeIn) {
        this.timeIn = timeIn;
    }

    public Date getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
    }

    public int getTotalHour() {
        return totalHour;
    }

    public void setTotalHour(int totalHour) {
        this.totalHour = totalHour;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(int salaryId) {
        this.salaryId = salaryId;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", date=" + date + ", timeIn=" + timeIn + ", timeOut=" + timeOut + ", totalHour=" + totalHour + ", userId=" + userId + ", salaryId=" + salaryId + '}';
    }

    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut checkInOut = new CheckInOut();
        try {
            checkInOut.setId(rs.getInt("checkIO_id"));
            checkInOut.setDate(rs.getDate("checkIO_date"));
            checkInOut.setTimeIn(rs.getTimestamp("checkIO_timeIn"));
            checkInOut.setTimeOut(rs.getDate("checkIO_timeOut"));
            checkInOut.setTotalHour(rs.getInt("checkIO_totalHr"));
            checkInOut.setUserId(rs.getInt("user_id"));
            checkInOut.setSalaryId(rs.getInt("salary_id"));

        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkInOut;
    }

}
