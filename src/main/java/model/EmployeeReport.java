package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmployeeReport {
    private String name;
    private int totalHours;

    public EmployeeReport(String name, int totalHours) {
        this.name = name;
        this.totalHours = totalHours;
    }

    public EmployeeReport() {
         this("", 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(int totalHours) {
        this.totalHours = totalHours;
    }

    @Override
    public String toString() {
        return "EmployeeReport{" + "name=" + name + ", totalHours=" + totalHours + '}';
    }
    
    public static EmployeeReport fromRS(ResultSet rs) {
        EmployeeReport employeeReport = new EmployeeReport();
        try {
            employeeReport.setName(rs.getString("Name"));
            employeeReport.setTotalHours(rs.getInt("Total_Hours"));
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employeeReport;
    }
    
    
}
