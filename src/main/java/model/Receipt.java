package model;

import Dao.CustomerDao;
import Dao.PromotionDao;
import Dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Receipt {

    private int id;
    private Date createdDate;
    private double total;
    private double recrived;
    private double changed;
    private String payment;
    private int usedPoint;
    private int recvPoint;
    private int UserId;
    private int cusId;
    private int promoId;
    private User user;
    private Customer customer;
    private Promotion promotion;
    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList();
    private ArrayList<Receipt> receipts = new ArrayList();
    private Receipt receipt;

    public Receipt(int id, Date createdDate, double total, double recrived, double changed, String payment,
            int usedPoint, int recvPoint, int UserId, int cusId, int promoId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.recrived = recrived;
        this.changed = changed;
        this.payment = payment;
        this.usedPoint = usedPoint;
        this.recvPoint = recvPoint;
        this.UserId = UserId;
        this.cusId = cusId;
        this.promoId = promoId;
    }

    public Receipt(Date createdDate, double total, double recrived, double changed, String payment, int usedPoint,
            int recvPoint, int UserId, int cusId, int promoId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.recrived = recrived;
        this.changed = changed;
        this.payment = payment;
        this.usedPoint = usedPoint;
        this.recvPoint = recvPoint;
        this.UserId = UserId;
        this.cusId = cusId;
        this.promoId = promoId;
    }

    public Receipt() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.recrived = 0;
        this.changed = 0;
        this.payment = "";
        this.usedPoint = 0;
        this.recvPoint = 0;
        this.UserId = 0;
        this.cusId = 0;
        this.promoId = 0;
    }

    public Receipt(double total, double recrived, double changed, String payment, int usedPoint, int recvPoint,
            int UserId, int cusId, int promoId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.recrived = recrived;
        this.changed = changed;
        this.payment = payment;
        this.usedPoint = usedPoint;
        this.recvPoint = recvPoint;
        this.UserId = UserId;
        this.cusId = cusId;
        this.promoId = promoId;
    }

    public Receipt(double recrived, double changed, String payment, int usedPoint, int recvPoint, int UserId, int cusId,
            int promoId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.recrived = recrived;
        this.changed = changed;
        this.payment = payment;
        this.usedPoint = usedPoint;
        this.recvPoint = recvPoint;
        this.UserId = UserId;
        this.cusId = cusId;
        this.promoId = promoId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getRecrived() {
        return recrived;
    }

    public void setRecrived(double recrived) {
        this.recrived = recrived;
    }

    public double getChanged() {
        return changed;
    }

    public void setChanged(double changed) {
        this.changed = changed;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public int getUsedPoint() {
        return usedPoint;
    }

    public void setUsedPoint(int usedPoint) {
        this.usedPoint = usedPoint;
    }

    public int getRecvPoint() {
        return recvPoint;
    }

    public void setRecvPoint(int recvPoint) {
        this.recvPoint = recvPoint;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public int getPromoId() {
        return promoId;
    }

    public void setPromoId(int promoId) {
        this.promoId = promoId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.UserId = user.getId();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.cusId = customer.getId();
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

    public void setReceiptDetails(ArrayList receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", recrived=" + recrived
                + ", changed=" + changed + ", payment=" + payment + ", usedPoint=" + usedPoint + ", recvPoint="
                + recvPoint + ", UserId=" + UserId + ", cusId=" + cusId + ", promoId=" + promoId + ", user=" + user
                + ", customer=" + customer + ", promotion=" + promotion + ", receiptDetails=" + receiptDetails + '}';
    }

    public void addReceiptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.add(receiptDetail);
        calculateTotal();
    }

    public void addReceiptDetail(Product product, int qty) {
        boolean updated = false;

        for (ReceiptDetail rd : receiptDetails) {
            if (rd.getProduct().getId() == product.getId()) {
                int newQty = rd.getQty() + qty;
                rd.setQty(newQty);
                rd.setTotalPrice(product.getPrice() * newQty);
                updated = true;
                break;
            }
        }

        if (!updated) {
            ReceiptDetail rd = new ReceiptDetail();
            rd.setProId(product.getId());
            rd.setProduct(product);
            rd.setTotalPrice(product.getPrice() * qty);
            rd.setQty(qty);
            receiptDetails.add(rd);
        }
        calculateTotal();
    }

    public void delRecieptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.remove(receiptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        double total = 0;
        for (ReceiptDetail rd : receiptDetails) {
            total += rd.getTotalPrice();

        }
        this.total = total;

    }

 public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("rec_id"));
            receipt.setCreatedDate(rs.getTimestamp("created_date"));
            receipt.setTotal(rs.getDouble("total_price"));
            receipt.setRecrived(rs.getDouble("received"));
            receipt.setChanged(rs.getDouble("changed"));
            receipt.setPayment(rs.getString("payment"));
            receipt.setUsedPoint(rs.getInt("used_point"));
            receipt.setRecvPoint(rs.getInt("recv_point"));
            receipt.setUserId(rs.getInt("user_id"));
            receipt.setCusId(rs.getInt("cus_id"));
            receipt.setPromoId(rs.getInt("promo_id"));

            if (receipt.getUserId() != 0) {
                UserDao userDao = new UserDao();
                User user = userDao.get(receipt.getUserId());
                receipt.setUser(user);
            }
            if (receipt.getCusId() != 0) {
                CustomerDao customerDao = new CustomerDao();
                Customer customer = customerDao.get(receipt.getCusId());
                receipt.setCustomer(customer);
            }
            if (receipt.getPromoId() != 0) {
                PromotionDao promotionDao = new PromotionDao();
                Promotion promotion = promotionDao.get(receipt.getPromoId());
                receipt.setPromotion(promotion);
            }
            return receipt;

        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
