package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CostRentReport {

    private Date month;
    private double rentCost;

    public CostRentReport(Date month, double rentCost) {
        this.month = month;
        this.rentCost = rentCost;
    }

    public CostRentReport() {
        this(null, 0);
    }

    public Date getMonth() {
        return month;
    }

    public void setMonth(Date month) {
        this.month = month;
    }

    public double getRentCost() {
        return rentCost;
    }

    public void setRentCost(double rentCost) {
        this.rentCost = rentCost;
    }

    @Override
    public String toString() {
        return "CostRentReport{" + "month=" + month + ", rentCost=" + rentCost + '}';
    }

    public static CostRentReport fromRS(ResultSet rs) {
        CostRentReport costrentReport = new CostRentReport();
        try {
            String dateString = rs.getString("Month");
            if (dateString != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = dateFormat.parse(dateString);
                costrentReport.setMonth(date);
            }
            costrentReport.setRentCost(rs.getDouble("Rent_Cost"));
        } catch (SQLException | ParseException ex) {
            Logger.getLogger(CostRentReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return costrentReport;
    }

}
