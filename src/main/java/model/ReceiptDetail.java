package model;

import Dao.ProductDao;
import Dao.ReceiptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import service.ProductService;

public class ReceiptDetail {

    private int id;
    private double totalPrice;
    private int qty;
    private int proId;
    private int recId;
    private Product product;
    private Receipt reciept;

    public ReceiptDetail(int id, double totalPrice, int qty, int proId, int recId) {
        this.id = id;
        this.totalPrice = totalPrice;
        this.qty = qty;
        this.proId = proId;
        this.recId = recId;
    }

    public ReceiptDetail(double totalPrice, int qty, int proId, int recId) {
        this.id = -1;
        this.totalPrice = totalPrice;
        this.qty = qty;
        this.proId = proId;
        this.recId = recId;
    }

    public ReceiptDetail() {
        this.id = -1;
        this.totalPrice = 0;
        this.qty = 0;
        this.proId = 0;
        this.recId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        ProductService productService = new ProductService();
        this.product = productService.getById(proId);
        if (product != null) {
            this.qty = qty;
            totalPrice = qty * product.getPrice();
        }
    }

    public int getProId() {
        return proId;
    }

    public void setProId(int proId) {
        this.proId = proId;
    }

    public int getRecId() {
        return recId;
    }

    public void setRecId(int recId) {
        this.recId = recId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Receipt getReciept() {
        return reciept;
    }

    public void setReciept(Receipt reciept) {
        this.reciept = reciept;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "id=" + id + ", totalPrice=" + totalPrice + ", qty=" + qty + ", proId=" + proId + ", recId=" + recId + ", product=" + product + ", reciept=" + reciept + '}';
    }

  public static ReceiptDetail fromRS(ResultSet rs) {
    ReceiptDetail receiptDetail = new ReceiptDetail();
    try {
        receiptDetail.setId(rs.getInt("orderlist_id"));
        receiptDetail.setTotalPrice(rs.getDouble("total_price"));
        receiptDetail.setQty(rs.getInt("qty"));
        receiptDetail.setProId(rs.getInt("pro_id"));
        receiptDetail.setRecId(rs.getInt("rec_id"));
        return receiptDetail;
    } catch (SQLException ex) {
        Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
        return null;
    }
}



}
