package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SalaryRecord {
    private int id;
    private Date date;
    private int total_Hr;
    private int total_Pay;
    private int userId;

    public SalaryRecord(int id, Date date, int total_Hr, int total_Pay, int userId) {
        this.id = id;
        this.date = date;
        this.total_Hr = total_Hr;
        this.total_Pay = total_Pay;
        this.userId = userId;
    }

      public SalaryRecord(int total_Hr, int total_Pay, int userId) {
        this.id = -1;
        this.date = null;
        this.total_Hr = total_Hr;
        this.total_Pay = total_Pay;
        this.userId = userId;
    }
      
         public SalaryRecord() {
        this.id = -1;
        this.date = null;
        this.total_Hr = 0;
        this.total_Pay = 0;
        this.userId = 0;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTotal_Hr() {
        return total_Hr;
    }

    public void setTotal_Hr(int total_Hr) {
        this.total_Hr = total_Hr;
    }

    public int getTotal_Pay() {
        return total_Pay;
    }

    public void setTotal_Pay(int total_Pay) {
        this.total_Pay = total_Pay;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "SalaryRecord{" + "id=" + id + ", date=" + date + ", total_Hr=" + total_Hr + ", total_Pay=" + total_Pay + ", userId=" + userId + '}';
    }
    
     public static SalaryRecord fromRS(ResultSet rs) {
        SalaryRecord sr = new SalaryRecord();
        try {
            sr.setId(rs.getInt("record_id"));
            sr.setDate(rs.getDate("pay_date"));
            sr.setTotal_Hr(rs.getInt("total_Hrs"));
            sr.setTotal_Pay(rs.getInt("total_pay"));
            sr.setUserId(rs.getInt("user_id"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return sr;
    }
}
