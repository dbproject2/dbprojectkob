package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReceiptDetailReport {

    private int id;
    private String name;
    private double sales;
    private int qty;

    public ReceiptDetailReport(int id, String name, double sales, int qty) {
        this.id = id;
        this.name = name;
        this.sales = sales;
        this.qty = qty;
    }

    public ReceiptDetailReport() {
        this(-1,"",0,0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSales() {
        return sales;
    }

    public void setSales(double sales) {
        this.sales = sales;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "ReceiptDetailReport{" + "id=" + id + ", name=" + name + ", sales=" + sales + ", qty=" + qty + '}';
    }
 
    public static ReceiptDetailReport fromRS(ResultSet rs) {
        ReceiptDetailReport receiptDetailReport = new ReceiptDetailReport();
        try {
            receiptDetailReport.setId(rs.getInt("ID"));
            receiptDetailReport.setName(rs.getString("Name"));
            receiptDetailReport.setSales(rs.getInt("Sales"));
            receiptDetailReport.setQty(rs.getInt("Qty"));
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDetailReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptDetailReport;
    }

}
