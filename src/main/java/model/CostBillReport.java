package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CostBillReport {

    private Date date;
    private double matCost;

    public CostBillReport(Date date, double matCost) {
        this.date = date;
        this.matCost = matCost;
    }

    public CostBillReport() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getMatCost() {
        return matCost;
    }

    public void setMatCost(double matCost) {
        this.matCost = matCost;
    }

    @Override
    public String toString() {
        return "CostBillReport{" + "date=" + date + ", matCost=" + matCost + '}';
    }

    public static CostBillReport fromRS(ResultSet rs) {
        CostBillReport costbillReport = new CostBillReport();
        try {
            String dateString = rs.getString("Month");
            if (dateString != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = dateFormat.parse(dateString);
                costbillReport.setDate(new Timestamp(date.getTime()));
            }
            costbillReport.setMatCost(rs.getDouble("Material_Cost"));
        } catch (SQLException | ParseException ex) {
            Logger.getLogger(CostBillReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return costbillReport;
    }

}
