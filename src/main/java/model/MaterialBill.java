package model;

import java.time.LocalTime;

public class MaterialBill {

    private int id;
    private String name;
    private float qty;
    private LocalTime date;
    private int billId;
    private float billPrice;
    private float totalPrice;

    public MaterialBill(int id, String name, float qty, int billId, float billPrice, float totalPrice) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.billId = billId;
        this.billPrice = billPrice;
        this.totalPrice = totalPrice;
        this.date = LocalTime.now();
    }

    public MaterialBill(String name, float qty, float billPrice, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.qty = qty;
        this.billId = -1;
        this.billPrice = billPrice;
        this.totalPrice = totalPrice;
        this.date = LocalTime.now();
    }

    public MaterialBill() {
        this.id = -1;
        this.name = "";
        this.qty = 0.0f;
        this.billId = -1;
        this.billPrice = 0.0f;
        this.totalPrice = 0.0f;
        this.date = LocalTime.now();
    }

    public MaterialBill(String name, float quantity) {
        this.id = -1;
        this.name = name;
        this.qty = quantity;
        this.billId = -1;
        this.billPrice = 0.0f;
        this.totalPrice = 0.0f;
        this.date = LocalTime.now();
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getQty() {
        return qty;
    }

    public void setQty(float qty) {
        this.qty = qty;
    }

    public LocalTime getDate() {
        return date;
    }

    public void setDate(LocalTime date) {
        this.date = date;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public float getBillPrice() {
        return billPrice;
    }

    public void setBillPrice(float billPrice) {
        this.billPrice = billPrice;
    }

    @Override
    public String toString() {
        return "MaterialBill{" + "id=" + id + ", name=" + name + ", qty=" + qty + ", date=" + date + ", billId=" + billId + ", billPrice=" + billPrice + ", totalPrice=" + totalPrice + '}';
    }
}
