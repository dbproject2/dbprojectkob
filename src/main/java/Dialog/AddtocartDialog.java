package Dialog;

import component.BuyProductable;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import model.Product;
import model.ReceiptDetail;
import service.ProductService;
import service.ReceiptService;

public class AddtocartDialog extends javax.swing.JDialog implements BuyProductable {

    private int id;
    private Product products;
    private ProductService productService;
    private ArrayList<BuyProductable> subscribers;
    private Product product;
    private Iterable<ReceiptDetail> receiptDetails;
    private ReceiptDetail receiptDetail;
    private ReceiptService receiptService;
    private int num = 0, sizeNum = 0;

    public AddtocartDialog(java.awt.Frame parent, int id) {
        super(parent, true);
        initComponents();
        subscribers = new ArrayList<>();
        this.id = id;
        productService = new ProductService();
        product = new Product();
        InitialProduct();
        ImageIcon icon = new ImageIcon("./pictures/product" + products.getId() + ".png");
        Image image = icon.getImage();
        Image newImage = image.getScaledInstance(150, 150, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblHeader.setIcon(icon);
    }

    public void disableButton() {
        rdHot.setEnabled(false);
        rdIce.setEnabled(false);
        rdMix.setEnabled(false);
        rdSizeS.setEnabled(false);
        rdSizeM.setEnabled(false);
        rdSizeL.setEnabled(false);
        cmbLevel.setEnabled(false);
    }

    private void InitialProduct() {
        products = productService.getById(id);
        lblName.setText("Name : " + products.getName());
        lblPrice.setText("Price : " + Float.toString(products.getPrice()));
        if (products.getName().equals("Water Bottle") || products.getCatId() == 2) {
            disableButton();
        }
    }

    public void AddOnByProduct(BuyProductable subsciber) {
        subscribers.add(subsciber);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSize = new javax.swing.ButtonGroup();
        btnType = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblPrice = new javax.swing.JLabel();
        lblSize = new javax.swing.JLabel();
        rdSizeS = new javax.swing.JRadioButton();
        rdSizeM = new javax.swing.JRadioButton();
        rdSizeL = new javax.swing.JRadioButton();
        lblType = new javax.swing.JLabel();
        rdHot = new javax.swing.JRadioButton();
        rdIce = new javax.swing.JRadioButton();
        rdMix = new javax.swing.JRadioButton();
        lblHeader = new javax.swing.JLabel();
        lblSweetLevel = new javax.swing.JLabel();
        cmbLevel = new javax.swing.JComboBox<>();
        btnClear = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(242, 239, 234));

        lblName.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        lblName.setText("Product name : ");

        lblPrice.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        lblPrice.setText("Price :");

        lblSize.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        lblSize.setText("Size :");

        btnSize.add(rdSizeS);
        rdSizeS.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        rdSizeS.setSelected(true);
        rdSizeS.setText("Size S");
        rdSizeS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdSizeSActionPerformed(evt);
            }
        });

        btnSize.add(rdSizeM);
        rdSizeM.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        rdSizeM.setText("Size M");
        rdSizeM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdSizeMActionPerformed(evt);
            }
        });

        btnSize.add(rdSizeL);
        rdSizeL.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        rdSizeL.setText("Size L");
        rdSizeL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdSizeLActionPerformed(evt);
            }
        });

        lblType.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        lblType.setText("Type :");

        btnType.add(rdHot);
        rdHot.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        rdHot.setSelected(true);
        rdHot.setText("Hot");
        rdHot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdHotActionPerformed(evt);
            }
        });

        btnType.add(rdIce);
        rdIce.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        rdIce.setText("Ice");
        rdIce.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdIceActionPerformed(evt);
            }
        });

        btnType.add(rdMix);
        rdMix.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        rdMix.setText("Mix");
        rdMix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdMixActionPerformed(evt);
            }
        });

        lblHeader.setBackground(new java.awt.Color(255, 255, 255));
        lblHeader.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        lblHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHeader.setOpaque(true);

        lblSweetLevel.setFont(new java.awt.Font("PT Serif Caption", 0, 12)); // NOI18N
        lblSweetLevel.setText("Sweet Level :");

        cmbLevel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0 %", "25 %", "50%", "75%", "100%" }));

        btnClear.setBackground(new java.awt.Color(153, 51, 0));
        btnClear.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnClear.setForeground(new java.awt.Color(255, 255, 255));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(104, 80, 51));
        btnSave.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(255, 255, 255));
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(lblHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblSweetLevel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblSize, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rdSizeS)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rdSizeM)
                                .addGap(18, 18, 18)
                                .addComponent(rdSizeL))
                            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(50, 50, 50)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblType)
                                .addGap(18, 18, 18)
                                .addComponent(rdHot)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rdIce)
                                .addGap(18, 18, 18)
                                .addComponent(rdMix))
                            .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(31, 31, 31)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(35, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rdSizeS)
                    .addComponent(rdSizeM)
                    .addComponent(rdSizeL)
                    .addComponent(lblType, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdHot)
                    .addComponent(rdIce)
                    .addComponent(rdMix))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSweetLevel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(lblHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(218, 208, 194));

        jLabel1.setFont(new java.awt.Font("PT Serif Caption", 1, 24)); // NOI18N
        jLabel1.setText("ADD TO CART");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rdMixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdMixActionPerformed
        if (rdMix.isSelected() & num == 0) {
            products.setPrice(products.calculatePrice(products.getPrice(), 10));
            rdHot.setSelected(false);
            rdIce.setSelected(false);
            num += 1;
        }
    }//GEN-LAST:event_rdMixActionPerformed

    private void rdIceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdIceActionPerformed
        if (rdIce.isSelected() & num == 0) {
            products.setPrice(products.calculatePrice(products.getPrice(), 5));
            System.out.println(products);
            rdHot.setSelected(false);
            rdMix.setSelected(false);
            num += 1;
        }
    }//GEN-LAST:event_rdIceActionPerformed

    private void rdHotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdHotActionPerformed
        if (rdHot.isSelected() & num == 0) {
            rdIce.setSelected(false);
            rdMix.setSelected(false);
        }
    }//GEN-LAST:event_rdHotActionPerformed

    private void rdSizeLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdSizeLActionPerformed
       if (rdSizeL.isSelected() & sizeNum == 0) {
            products.setPrice(products.calculatePrice(products.getPrice(), 10));
            System.out.println(products);
            rdSizeS.setSelected(false);
            rdSizeM.setSelected(false);
            sizeNum += 1;
    }//GEN-LAST:event_rdSizeLActionPerformed
    }
    private void rdSizeSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdSizeSActionPerformed
        if (rdSizeS.isSelected()) {
            rdSizeM.setSelected(false);
            rdSizeL.setSelected(false);
        }
    }//GEN-LAST:event_rdSizeSActionPerformed

    private void rdSizeMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdSizeMActionPerformed
        if (rdSizeM.isSelected() & sizeNum == 0) {
            products.setPrice(products.calculatePrice(products.getPrice(), 5));
            System.out.println(products);
            rdSizeS.setSelected(false);
            rdSizeL.setSelected(false);
            sizeNum += 1;
        }
    }//GEN-LAST:event_rdSizeMActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {
        productService = new ProductService();
        System.out.println(products);
        buy(products, 1);
        clearForm();
        dispose();
    }

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {
        clearForm();
    }

    private void clearForm() {
        rdSizeS.setSelected(false);
        rdSizeM.setSelected(false);
        rdSizeL.setSelected(false);
        cmbLevel.setSelectedIndex(0);
        rdHot.setSelected(false);
        rdIce.setSelected(false);
        rdMix.setSelected(false);
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddtocartDialog.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddtocartDialog.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddtocartDialog.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddtocartDialog.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.ButtonGroup btnSize;
    private javax.swing.ButtonGroup btnType;
    private javax.swing.JComboBox<String> cmbLevel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblHeader;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblSize;
    private javax.swing.JLabel lblSweetLevel;
    private javax.swing.JLabel lblType;
    private javax.swing.JRadioButton rdHot;
    private javax.swing.JRadioButton rdIce;
    private javax.swing.JRadioButton rdMix;
    private javax.swing.JRadioButton rdSizeL;
    private javax.swing.JRadioButton rdSizeM;
    private javax.swing.JRadioButton rdSizeS;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        for (BuyProductable s : subscribers) {
            s.buy(product, qty);
        }
    }
}
