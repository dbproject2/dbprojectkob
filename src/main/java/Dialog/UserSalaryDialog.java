package Dialog;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import model.CheckInOut;
import model.SummarySalary;
import model.User;
import service.CheckInoutService;
import service.SummarySalaryService;

public class UserSalaryDialog extends javax.swing.JDialog {

    private User editedUser;
    private List<CheckInOut> list;
    private String status;

    public UserSalaryDialog(java.awt.Frame parent, User editedUser) {
        super(parent, true);
        initComponents();
        CheckInOut io = new CheckInOut();
        this.editedUser = editedUser;
        lblName.setText("Name : " + editedUser.getName());
        InitTable();
    }

    public void InitTable() {
        CheckInoutService cs = new CheckInoutService();
        list = cs.getCheckInOutsById(editedUser.getId());
        tblUserSalary.setModel(new AbstractTableModel() {
            String[] columnNames = {"Date", "Time-In", "Time-Out", "Total Hour", "Costs", "Status"};

            @Override
            public int getRowCount() {
                return cs.getCheckInOutsById(editedUser.getId()).size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckInOut checkInOut = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return checkInOut.getDate();
                    case 1:
                        return checkInOut.getTimeIn();
                    case 2:
                        return checkInOut.getTimeOut();
                    case 3:
                        return checkInOut.getTotalHour();
                    case 4:
                        return checkInOut.getSalaryId();
                    default:
                        return "Overdue";
                }
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
        });
    }

    public void InitTable(int id) {
        CheckInoutService cs = new CheckInoutService();
        list = cs.getCheckInOutsById(editedUser.getId());
        tblUserSalary.setModel(new AbstractTableModel() {
            String[] columnNames = {"Date", "Time-In", "Time-Out", "Total Hour", "Costs", "Status"};

            @Override
            public int getRowCount() {
                return cs.getCheckInOutsById(editedUser.getId()).size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckInOut checkInOut = list.get(rowIndex);
                SummarySalaryService ss = new SummarySalaryService();
                switch (columnIndex) {
                    case 0:
                        return checkInOut.getDate();
                    case 1:
                        return checkInOut.getTimeIn();
                    case 2:
                        return checkInOut.getTimeOut();
                    case 3:
                        return checkInOut.getTotalHour();
                    case 4:
                        return checkInOut.getSalaryId();
                    case 5:
                        if (ss.getSalaryById(id).getStatus() == "NO") {
                            return 0;
                        }
                        return "Paid";
                    default:
                        return "Overdue";
                }
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnPrint = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblUserSalary = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("UserSalary");

        jPanel1.setBackground(new java.awt.Color(54, 54, 53));

        lblName.setFont(new java.awt.Font("PT Serif Caption", 1, 18)); // NOI18N
        lblName.setForeground(new java.awt.Color(255, 255, 255));
        lblName.setText("Name");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(82, 82, 80));

        btnPrint.setBackground(new java.awt.Color(120, 163, 139));
        btnPrint.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnPrint.setForeground(new java.awt.Color(255, 255, 255));
        btnPrint.setText("Bill Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        tblUserSalary.setBackground(new java.awt.Color(46, 80, 70));
        tblUserSalary.setForeground(new java.awt.Color(255, 255, 255));
        tblUserSalary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblUserSalary);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(42, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        SummarySalaryService ss = new SummarySalaryService();
        openDialog();
        int id = editedUser.getId();
        SummarySalary ssp = ss.getSalaryById(id);
        ssp.setStatus("YES");
        ss.update(ssp);
        InitTable(id);
        refreshTable();
    }//GEN-LAST:event_btnPrintActionPerformed

    private void openDialog() {
        SummarySalary ss = new SummarySalary();
        SummarySalaryService sss = new SummarySalaryService();
        CheckInOut io = new CheckInOut();
        int hour = sss.getSalaryById(editedUser.getId()).getSalary_hr();
        String name = editedUser.getName();
        JDialog frame = (JDialog) SwingUtilities.getRoot(this);
        SalaryDialog salaryDialog = new SalaryDialog(frame, hour, name);
        salaryDialog.setLocationRelativeTo(this);
        salaryDialog.setVisible(true);
        salaryDialog.addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }

    public void refreshTable() {
        tblUserSalary.revalidate();
        tblUserSalary.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPrint;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblName;
    private javax.swing.JTable tblUserSalary;
    // End of variables declaration//GEN-END:variables
}
