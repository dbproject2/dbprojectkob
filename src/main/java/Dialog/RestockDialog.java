package Dialog;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import model.Material;
import model.MaterialBill;
import service.MaterialService;

public class RestockDialog extends javax.swing.JFrame {

    private MaterialService materialService;
    private List<Material> materialList;
    private List<Material> materialListAdd;

    private List<Material> list;
    private Material buyMaterial;
    private float qty = 0.0f;
    private float price = 0.0f;

    private List<MaterialBill> billList;
    private MaterialBill matBill;
    private List<MaterialBill> materialBill;
    private int billId;
    private int hisId;

    public RestockDialog(java.awt.Frame parent, List<Material> list, List<MaterialBill> materialBill, int billId) {
        this.list = list;
        materialService = new MaterialService();
        materialList = materialService.getMaterials();
        materialListAdd = new ArrayList<>();
        this.billList = new ArrayList<>();
        this.materialBill = materialBill;
        this.billId = billId;
        this.hisId = hisId;
        initComponents();
        tblMaterial.setRowHeight(65);
        tblBuyMaterial.setRowHeight(65);
        lblPrice.setText("Price : " + price);
        lblDiscount.setText("Discount : 0.0");
        lblTotalPrice.setText("Total Price : " + price);
        tblMaterial.setModel(new AbstractTableModel() {
            String[] columnNames = {"Order", "ID", "Name", "Min", ""};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return materialList.size();

            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 4) {
                    return ImageIcon.class;
                } else {
                    return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {

                Material material = materialList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return rowIndex + 1;
                    case 1:
                        return material.getId();
                    case 2:
                        return material.getName();
                    case 3:
                        return material.getMin();
                    case 4:
                        ImageIcon addIcon = new ImageIcon("./Icon/MatAdd.png");
                        Image addImage = addIcon.getImage();
                        Image newAddImage = addImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
                        addIcon.setImage(newAddImage);
                        return addIcon;

                    default:
                        return "UNKNOW";
                }
            }
        });
        showBuyTable();
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                onDialogClose();
            }
        });
    }

    public void showBuyTable() {
        tblBuyMaterial.setModel(new AbstractTableModel() {
            String[] columnNames = {"Name", "Quantity", "Price", ""};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                if (materialListAdd == null) {
                    return 0;
                } else {
                    return materialListAdd.size();
                }
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 3) {
                    return ImageIcon.class;
                } else if (columnIndex == 2 && columnIndex == 3) {
                    return float.class;
                } else {
                    return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {

                Material material = materialListAdd.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getName();
                    case 1:
                        return material.getQty();
                    case 2:
                        return material.getPricePerUnit() * material.getQty();
                    case 3:
                        ImageIcon deleteIcon = new ImageIcon("./Icon/MatDelete.png");
                        Image deleteImage = deleteIcon.getImage();
                        Image newDeleteImage = deleteImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
                        deleteIcon.setImage(newDeleteImage);
                        return deleteIcon;
                    default:
                        return "UNKNOW";
                }
            }
        });
    }

    private void onDialogClose() {
        dispose();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel5 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        lblPrice = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lblTotalPrice = new javax.swing.JLabel();
        btnAccept = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblBuyMaterial = new javax.swing.JTable();

        jLabel5.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        jLabel5.setText("jLabel3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(100, 137, 127));

        jPanel1.setBackground(new java.awt.Color(54, 54, 53));

        jLabel1.setFont(new java.awt.Font("PT Serif Caption", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Restock");

        jLabel2.setFont(new java.awt.Font("PT Serif Caption", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Date :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(94, 94, 92));

        jScrollPane3.setBackground(new java.awt.Color(46, 80, 70));
        jScrollPane3.setForeground(new java.awt.Color(255, 255, 255));

        tblMaterial.setBackground(new java.awt.Color(46, 80, 70));
        tblMaterial.setForeground(new java.awt.Color(255, 255, 255));
        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Order", "ID", "Name", "Min", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMaterialMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblMaterial);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 444, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(94, 94, 92));

        jPanel4.setBackground(new java.awt.Color(54, 54, 53));

        lblPrice.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        lblPrice.setForeground(new java.awt.Color(255, 255, 255));
        lblPrice.setText("Price :");

        lblDiscount.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        lblDiscount.setForeground(new java.awt.Color(255, 255, 255));
        lblDiscount.setText("Discount :");

        lblTotalPrice.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        lblTotalPrice.setForeground(new java.awt.Color(255, 255, 255));
        lblTotalPrice.setText("Total Price :");

        btnAccept.setBackground(new java.awt.Color(104, 80, 51));
        btnAccept.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnAccept.setForeground(new java.awt.Color(255, 255, 255));
        btnAccept.setText("Accept");
        btnAccept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcceptActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(153, 51, 0));
        btnCancel.setFont(new java.awt.Font("PT Serif Caption", 1, 14)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(lblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalPrice)
                    .addComponent(lblPrice))
                .addGap(28, 28, 28)
                .addComponent(lblDiscount)
                .addGap(15, 15, 15)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );

        jScrollPane4.setBackground(new java.awt.Color(46, 80, 70));
        jScrollPane4.setForeground(new java.awt.Color(255, 255, 255));

        tblBuyMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Quantity", "Price", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblBuyMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblBuyMaterialMouseClicked(evt);
            }
        });
        tblBuyMaterial.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tblBuyMaterialPropertyChange(evt);
            }
        });
        jScrollPane4.setViewportView(tblBuyMaterial);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane4)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMaterialMouseClicked
        int column = tblMaterial.columnAtPoint(evt.getPoint());
        int row = tblMaterial.rowAtPoint(evt.getPoint());
        price = 0.0f;
        if (column == 4) {
            Material materialToBuy = materialList.get(row);
            System.out.println(materialToBuy);
            JFrame frame = (JFrame) SwingUtilities.getRoot(this);
            BuyMaterialDialog buyMaterialDialog = new BuyMaterialDialog(frame, materialToBuy, materialListAdd);

            buyMaterialDialog.setLocationRelativeTo(this);
            buyMaterialDialog.setVisible(true);
            buyMaterialDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    refreshTable();
                    for (Material mat : materialListAdd) {
                        price += mat.getQty() * mat.getPricePerUnit();
                    }
                    lblPrice.setText("Price : " + price);
                    lblTotalPrice.setText("Total Price : " + price);
                }
            });
        }

    }//GEN-LAST:event_tblMaterialMouseClicked

    private void btnAcceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcceptActionPerformed
        for (Material mat : list) {
            for (Material mat2 : materialListAdd) {
                if (mat.getId() == mat2.getId()) {
                    System.out.println("Can Update");
                    float qty1 = mat.getQty();
                    float qty2 = mat2.getQty();
                    mat.setQty(qty1 + qty2);
                    System.out.println(mat);
                    materialService.update(mat);
                    matBill = new MaterialBill(mat.getId(), mat.getName(), mat2.getQty(), billId, mat2.getQty() * mat.getPricePerUnit(), price);
                    billList.add(matBill);
                    materialBill.add(matBill);
                    System.out.println("-------Test Bill------");
                    for (MaterialBill mb : billList) {
                        System.out.println(mb);
                    }
                    System.out.println("------Test Mat Bill------");
                    for (MaterialBill mb : materialBill) {
                        System.out.println(mb);
                    }

                }

            }
        }
        dispose();
    }//GEN-LAST:event_btnAcceptActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void tblBuyMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblBuyMaterialMouseClicked
        int column = tblBuyMaterial.columnAtPoint(evt.getPoint());
        int row = tblBuyMaterial.rowAtPoint(evt.getPoint());
        if (column == 3) {
            Material materialToDelete = materialListAdd.get(row);
            System.out.println(materialToDelete);

            price -= materialToDelete.getQty() * materialToDelete.getPricePerUnit();

            lblPrice.setText("Price : " + price);
            lblTotalPrice.setText("Total Price : " + price);

            materialListAdd.remove(materialToDelete);
            refreshTable();

        }
    }//GEN-LAST:event_tblBuyMaterialMouseClicked

    private void tblBuyMaterialPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tblBuyMaterialPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_tblBuyMaterialPropertyChange

    private void refreshTable() {
        tblBuyMaterial.revalidate();
        tblBuyMaterial.repaint();

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAccept;
    private javax.swing.JButton btnCancel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblTotalPrice;
    private javax.swing.JTable tblBuyMaterial;
    private javax.swing.JTable tblMaterial;
    // End of variables declaration//GEN-END:variables
}
