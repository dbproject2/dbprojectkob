package ui;

import Panel.CustomerPanel;
import Panel.HistoryReceiptPanel;
import Panel.MaterialPanel;
import Panel.PaywagePanel;
import Panel.PosPanel;
import Panel.ProductPanel;
import Panel.RentalPanel;
import Panel.ReportPanel;
import Panel.UserPanel;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import model.CheckInOut;
import service.CheckInoutService;

public class MainFrame extends javax.swing.JFrame {

    public MainFrame(String username, String role) {
        initComponents();
        setTitle("Point of Sell");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pnlSrc.setViewportView(new PosPanel());
        ImageIcon userImageIcon = new ImageIcon("./Icon/LogoDcoffee.png"); 
        Image userImage = userImageIcon.getImage().getScaledInstance(100, 100 ,Image.SCALE_SMOOTH);
        lblLogo.setIcon(new ImageIcon(userImage));
        jPanel2.add(lblName);
        jPanel2.add(lblRole);
        showUserInformation(username, role);

    }
    
    private void showUserInformation(String username, String role) {
        lblName.setText("Name: " + username);
        lblRole.setText("Role: " + role);
    }

    public void showOwnerMenu() {
        btnProduct.setVisible(true);
        btnPos.setVisible(true);
        btnCust.setVisible(true);
        btnMeteria.setVisible(true);
        btnUser.setVisible(true);
        ImageIcon userImageIcon = new ImageIcon("./User/Owner.png"); 
        Image userImage = userImageIcon.getImage().getScaledInstance(100, 100 ,Image.SCALE_SMOOTH);
        lblPic.setIcon(new ImageIcon(userImage));
        
    }

    public void showEmployeeMenu() {
        btnProduct.setVisible(false);
        btnPos.setVisible(true);
        btnCust.setVisible(false);
        btnMeteria.setVisible(false);
        btnUser.setVisible(false);
        ImageIcon userImageIcon = new ImageIcon("./User/Employee.png"); 
        Image userImage = userImageIcon.getImage().getScaledInstance(100, 100 ,Image.SCALE_SMOOTH);
        lblPic.setIcon(new ImageIcon(userImage));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblRole = new javax.swing.JLabel();
        lblPic = new javax.swing.JLabel();
        lblLogo = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btnProduct = new javax.swing.JButton();
        btnPos = new javax.swing.JButton();
        btnCust = new javax.swing.JButton();
        btnMeteria = new javax.swing.JButton();
        btnUser = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        btnPAywage = new javax.swing.JButton();
        btnRental = new javax.swing.JButton();
        btnReport = new javax.swing.JButton();
        btnHisRe = new javax.swing.JButton();
        pnlSrc = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(1335, 135));

        jPanel1.setBackground(new java.awt.Color(218, 208, 194));
        jPanel1.setMaximumSize(new java.awt.Dimension(1280, 720));

        jPanel2.setBackground(new java.awt.Color(26, 77, 67));
        jPanel2.setMaximumSize(new java.awt.Dimension(1335, 135));

        lblName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblName.setForeground(new java.awt.Color(255, 255, 255));
        lblName.setText("Name");

        lblRole.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblRole.setForeground(new java.awt.Color(255, 255, 255));
        lblRole.setText("Role");

        lblPic.setMaximumSize(new java.awt.Dimension(100, 100));
        lblPic.setPreferredSize(new java.awt.Dimension(100, 100));

        lblLogo.setMaximumSize(new java.awt.Dimension(100, 100));
        lblLogo.setPreferredSize(new java.awt.Dimension(100, 100));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblPic, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(212, 212, 212)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRole, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(82, 82, 82))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblRole, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblPic, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(26, 77, 67));
        jPanel6.setMaximumSize(new java.awt.Dimension(166, 563));

        btnProduct.setBackground(new java.awt.Color(46, 80, 70));
        btnProduct.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnProduct.setForeground(new java.awt.Color(255, 255, 255));
        btnProduct.setText("Product");
        btnProduct.setMaximumSize(new java.awt.Dimension(137, 36));
        btnProduct.setPreferredSize(new java.awt.Dimension(137, 36));
        btnProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductActionPerformed(evt);
            }
        });

        btnPos.setBackground(new java.awt.Color(46, 80, 70));
        btnPos.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnPos.setForeground(new java.awt.Color(255, 255, 255));
        btnPos.setText("Point of Sell");
        btnPos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPosActionPerformed(evt);
            }
        });

        btnCust.setBackground(new java.awt.Color(46, 80, 70));
        btnCust.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnCust.setForeground(new java.awt.Color(255, 255, 255));
        btnCust.setText("Customer");
        btnCust.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustActionPerformed(evt);
            }
        });

        btnMeteria.setBackground(new java.awt.Color(46, 80, 70));
        btnMeteria.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnMeteria.setForeground(new java.awt.Color(255, 255, 255));
        btnMeteria.setText("Material");
        btnMeteria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMeteriaActionPerformed(evt);
            }
        });

        btnUser.setBackground(new java.awt.Color(46, 80, 70));
        btnUser.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnUser.setForeground(new java.awt.Color(255, 255, 255));
        btnUser.setText("User");
        btnUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserActionPerformed(evt);
            }
        });

        btnLogout.setBackground(new java.awt.Color(46, 80, 70));
        btnLogout.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnLogout.setForeground(new java.awt.Color(255, 255, 255));
        btnLogout.setText("Log-out");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        btnPAywage.setBackground(new java.awt.Color(46, 80, 70));
        btnPAywage.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnPAywage.setForeground(new java.awt.Color(255, 255, 255));
        btnPAywage.setText("Pay Wage");
        btnPAywage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPAywageActionPerformed(evt);
            }
        });

        btnRental.setBackground(new java.awt.Color(46, 80, 70));
        btnRental.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnRental.setForeground(new java.awt.Color(255, 255, 255));
        btnRental.setText("Rental");
        btnRental.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRentalActionPerformed(evt);
            }
        });

        btnReport.setBackground(new java.awt.Color(46, 80, 70));
        btnReport.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnReport.setForeground(new java.awt.Color(255, 255, 255));
        btnReport.setText("Dashboard");
        btnReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportActionPerformed(evt);
            }
        });

        btnHisRe.setBackground(new java.awt.Color(46, 80, 70));
        btnHisRe.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnHisRe.setForeground(new java.awt.Color(255, 255, 255));
        btnHisRe.setText("History Receipt");
        btnHisRe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHisReActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnPos, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                    .addComponent(btnProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnMeteria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCust, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPAywage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnRental, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnLogout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnHisRe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnPos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMeteria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCust)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPAywage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRental)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnReport)
                .addGap(4, 4, 4)
                .addComponent(btnHisRe)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlSrc.setBackground(new java.awt.Color(66, 70, 66));
        pnlSrc.setMaximumSize(new java.awt.Dimension(1070, 563));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlSrc, javax.swing.GroupLayout.PREFERRED_SIZE, 1145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlSrc, javax.swing.GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        // TODO add your handling code here:
        CheckInOut io = new CheckInOut();
        CheckInoutService cs = new CheckInoutService();
        logOut();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserActionPerformed
        pnlSrc.setViewportView(new UserPanel());
        setTitle("User");
    }//GEN-LAST:event_btnUserActionPerformed

    private void btnMeteriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMeteriaActionPerformed
        pnlSrc.setViewportView(new MaterialPanel());
        setTitle("Material");
    }//GEN-LAST:event_btnMeteriaActionPerformed

    private void btnCustActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustActionPerformed
        pnlSrc.setViewportView(new CustomerPanel());
        setTitle("Customer");
    }//GEN-LAST:event_btnCustActionPerformed

    private void btnPosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPosActionPerformed
        pnlSrc.setViewportView(new PosPanel());
        setTitle("Point of Sell");

    }//GEN-LAST:event_btnPosActionPerformed

    private void btnProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductActionPerformed
        pnlSrc.setViewportView(new ProductPanel());
        setTitle("Product");

    }//GEN-LAST:event_btnProductActionPerformed

    private void btnPAywageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPAywageActionPerformed
        pnlSrc.setViewportView(new PaywagePanel());
        setTitle("Pay Wage");
    }//GEN-LAST:event_btnPAywageActionPerformed

    private void btnRentalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRentalActionPerformed
        pnlSrc.setViewportView(new RentalPanel());
        setTitle("Rental");

    }//GEN-LAST:event_btnRentalActionPerformed

    private void btnReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportActionPerformed
        pnlSrc.setViewportView(new ReportPanel());
        setTitle("Report");

    }//GEN-LAST:event_btnReportActionPerformed

    private void btnHisReActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHisReActionPerformed
       pnlSrc.setViewportView(new HistoryReceiptPanel());
        setTitle("History Receipt");
    }//GEN-LAST:event_btnHisReActionPerformed

    private void logOut() {
        this.dispose();
        LoginFrame lf = new LoginFrame();
        lf.setVisible(true);
        showUserInformation("Not Logged In", "");
    }

    public static void main(String args[]) {
        String username = "your_username_here";
        String role = "your_role_here";
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame(username, role).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCust;
    private javax.swing.JButton btnHisRe;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnMeteria;
    private javax.swing.JButton btnPAywage;
    private javax.swing.JButton btnPos;
    private javax.swing.JButton btnProduct;
    private javax.swing.JButton btnRental;
    private javax.swing.JButton btnReport;
    private javax.swing.JButton btnUser;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPic;
    private javax.swing.JLabel lblRole;
    private javax.swing.JScrollPane pnlSrc;
    // End of variables declaration//GEN-END:variables
}
